var React = require('react');
var ReactRouter = require('react-router');
var History = ReactRouter.History;
var Link = ReactRouter.Link;

var Home = React.createClass({
    getInitialState: function() {
        return {};
    },

    componentDidMount: function() {
        var query = this.props.location.query;
        if (query.hasOwnProperty('initTree') && this.isMounted()) {
            setTimeout(function() {
                $.AdminLTE.tree('.sidebar');
                this.history.replaceState(null, '/');
            }, 200);
        }
    },

    componentWillUnmount: function() {
    },

    render: function() {
        return (
            <div>
                <div className="content-header">
                    <h1>Dashboard</h1>
                    <ol className="breadcrumb">
                        <li><Link to='/'><i className="fa fa-dashboard"/> Home</Link></li>
                    </ol>
                </div>

                <div className="content body">
                    <div className="row">
                        <div className="col-lg-3 col-xs-6">
                            <div className="small-box bg-aqua">
                                <div className="inner">
                                    <h3>150</h3>
                                    <p>New Orders</p>
                                </div>
                                <div className="icon"><i className="ion ion-bag" /></div>
                                <a href="#" className="small-box-footer">More info <i className="fa fa-arrow-circle-right" /></a>
                            </div>
                        </div>
                        <div className="col-lg-3 col-xs-6">
                            <div className="small-box bg-green">
                                <div className="inner">
                                    <h3>53<sup style={{fontSize: 20 + 'px'}}>%</sup></h3>
                                    <p>Bounce Rate</p>
                                </div>
                                <div className="icon"><i className="ion ion-stats-bars" /></div>
                                <a href="#" className="small-box-footer">More info <i className="fa fa-arrow-circle-right" /></a>
                            </div>
                        </div>
                        <div className="col-lg-3 col-xs-6">
                            <div className="small-box bg-yellow">
                                <div className="inner">
                                    <h3>44</h3>
                                    <p>User Registrations</p>
                                </div>
                                <div className="icon"><i className="ion ion-person-add" /></div>
                                <a href="#" className="small-box-footer">More info <i className="fa fa-arrow-circle-right" /></a>
                            </div>
                        </div>
                        <div className="col-lg-3 col-xs-6">
                            <div className="small-box bg-red">
                                <div className="inner">
                                    <h3>65</h3>
                                    <p>Unique Visitors</p>
                                </div>
                                <div className="icon"><i className="ion ion-pie-graph" />
                                </div>
                                <a href="#" className="small-box-footer">More info <i className="fa fa-arrow-circle-right" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

});

module.exports = Home;