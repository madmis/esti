var React = require('react');
var ReactRouter = require('react-router');
var History = ReactRouter.History;
var Link = ReactRouter.Link;

var SessionStore = require('../stores/SessionStore.js');

var Profile = React.createClass({
    getInitialState: function() {
        return {
            user: SessionStore.getUser()
        };
    },
    render: function() {
        return (
            <div>
                <section className="content-header">
                    <h1>User Profile</h1>
                    <ol className="breadcrumb">
                        <li><Link to='/'><i className="fa fa-dashboard"/> Home</Link></li>
                        <li className="active">profile</li>
                    </ol>
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-md-3">

                            <div className="box box-primary">
                                <div className="box-body box-profile">
                                    <img className="profile-user-img img-responsive img-circle" src="http://placehold.it/128x128" alt="User profile picture" />
                                    <h3 className="profile-username text-center">{this.state.user.email}</h3>
                                    <p className="text-muted text-center">Software Engineer</p>
                                    <ul className="list-group list-group-unbordered">
                                        <li className="list-group-item"> <b>Followers</b> <a className="pull-right">1,322</a> </li>
                                        <li className="list-group-item"> <b>Following</b> <a className="pull-right">543</a> </li>
                                        <li className="list-group-item"> <b>Friends</b> <a className="pull-right">13,287</a> </li>
                                    </ul>

                                    <a href="#" className="btn btn-primary btn-block"><b>Follow</b></a>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-9"></div>
                    </div>
                </section>
            </div>
        );
    }
});

module.exports = Profile;