var React = require('react');
var BodyClassName = require('react-body-classname');
var ReactRouter = require('react-router');
var History = ReactRouter.History;
var Link = ReactRouter.Link;
var Forms = require('newforms');
var NotificationSystem = require('react-notification-system');
var request = require('superagent');
var TimerMixin = require('react-timer-mixin');

var Constants = require('../../constants/Constants.js');
var Const = Constants.Const;
var APIEndpoints = Constants.APIEndpoints;

var AppForms = require('../form/AppForms.js');
var NotificationsMixin = require('../../mixin/NotificationsMixin.js');
var UserActionCreators = require('../../actions/UserActionCreators.js');
var ResetPasswordStore = require('../../stores/ResetPasswordStore.js');
var SessionStore = require('../../stores/SessionStore.js');

var ResetForm = Forms.Form.extend({
    password: Forms.CharField({
        widget: Forms.PasswordInput,
        widgetAttrs: {
            'data-feedback': 'glyphicon-lock',
            className: 'form-control',
            placeholder: "Password",
            autoComplete: "off"
        }
    }),
    confirmPassword: Forms.CharField({
        widget: Forms.PasswordInput,
        widgetAttrs: {
            'data-feedback': 'glyphicon-lock',
            className: 'form-control',
            placeholder: "Retype password",
            autoComplete: "off"
        }
    }),
    csrfToken: Forms.CharField({
        required: false,
        widgetAttrs: {
            hidden: true
        }
    })
});

var ResetPassword = React.createClass({
    mixins: [ History, NotificationsMixin, TimerMixin ],
    getInitialState: function() {
        return {
            form: new ResetForm({
                controlled: true,
                onChange: this.forceUpdate.bind(this)
            })
        };
    },
    componentDidMount: function() {
        if (SessionStore.isLoggedIn()) {
            this.history.replaceState(null, `/`);
        }

        ResetPasswordStore.addChangeListener(this._onChange);

        this.setState({ token: this.props.params.token });

        request.get(APIEndpoints.GET_CSRF + '/reset-password')
            .set('Accept', 'application/json')
            .withCredentials()
            .end(function(error, response) {
                this.state.form.updateData({
                    csrfToken: response.body.data.csrf_token
                });
            }.bind(this));
    },
    componentWillUnmount: function() {
        ResetPasswordStore.removeChangeListener(this._onChange);
    },
    _onChange: function(event) {
        if (ResetPasswordStore.hasErrors()) {
            this.addErrors(ResetPasswordStore.getErrors());
        } else {
            this.addNotification(
                ResetPasswordStore.getResetMessage(),
                Const.NOTIFY_SUCCESS
            );
            this.setTimeout(
                function() { this.history.replaceState(null, `/login`); },
                2000
            );
        }
    },
    _onSubmit: function(e) {
        e.preventDefault();

        var form = this.state.form;
        var isValid = form.validate();
        if (isValid) {
            var data = form.cleanedData;
            data.token = this.state.token;
            UserActionCreators.resetPassword(data);
        }
    },
    render: function() {
        return (
            <BodyClassName className="hold-transition login-page">
                <div className="login-box">
                    <NotificationSystem ref="notificationSystem" />
                    <div className="login-logo">
                        <b>Esti</b> Change Password
                    </div>

                    <div className="login-box-body">
                        <p className="login-box-msg">Enter your new password</p>

                        <form onSubmit={this._onSubmit}>
                            {this.state.form.boundFields().map(AppForms.renderField)}

                            <div className="col-xs-6 col-xs-offset-6">
                                <button type="submit" className="btn btn-primary btn-block btn-flat">Reset Password</button>
                            </div>
                        </form>

                        <Link to='/login' activeClassName="active" className="text-center">I already have a membership</Link>
                        <br />
                        <Link to='/register' activeClassName="active" className="text-center">Register a new membership</Link>
                    </div>
                </div>
            </BodyClassName>
        );
    }
});

module.exports = ResetPassword;
