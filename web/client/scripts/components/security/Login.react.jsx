var React = require('react');
var BodyClassName = require('react-body-classname');
var ReactRouter = require('react-router');
var History = ReactRouter.History;
var Link = ReactRouter.Link;
var NotificationSystem = require('react-notification-system');
var Loader = require('react-loader');

var AppForms = require('../form/AppForms.js');
var LoginForm = require('../form/LoginForm.js');
var SessionStore = require('../../stores/SessionStore.js');
var UserActionCreators = require('../../actions/UserActionCreators.js');
var NotificationsMixin = require('../../mixin/NotificationsMixin.js');


var Login = React.createClass({
    mixins: [ History, NotificationsMixin ],
    getInitialState: function() {
        return {
            loaded: true,
            form: new LoginForm({
                controlled: true,
                onChange: this.forceUpdate.bind(this)
            })
        };
    },
    componentDidMount: function() {
        SessionStore.addChangeListener(this._onChange);
        this._notificationSystem = this.refs.notificationSystem;

        if (SessionStore.isLoggedIn()) {
            this.history.replaceState(null, '/?initTree');
        }
    },
    componentWillUnmount: function() {
        SessionStore.removeChangeListener(this._onChange);
    },
    _onChange: function(event) {
        this.setState({ loaded: true });
        if (SessionStore.isLoggedIn()) {
            this.history.replaceState(null, '/?initTree');
        } else {
            if (SessionStore.getErrors().length > 0) {
                this.addErrors(SessionStore.getErrors());
            }
        }
    },
    _onSubmit: function(e) {
        e.preventDefault();

        var form = this.state.form;
        var isValid = form.validate();
        if (isValid) {
            this.setState({ loaded: false });
            UserActionCreators.login(
                form.cleanedData.email,
                form.cleanedData.password
            );
        }
    },
    render: function() {
        return (
            <BodyClassName className="hold-transition login-page">
                <div className="login-box">
                    <Loader loaded={this.state.loaded} top="20px" left="50%" />
                    <NotificationSystem ref="notificationSystem" />

                    <div className="login-logo">
                        <b>Esti</b> Login
                    </div>

                    <div className="login-box-body">
                        <p className="login-box-msg">Sign in to start your session</p>

                        <form onSubmit={this._onSubmit}>
                            {this.state.form.boundFields().map(AppForms.renderField)}

                            <div className="col-xs-4 col-xs-offset-8">
                                <button type="submit" className="btn btn-primary btn-block btn-flat">Sign In</button>
                            </div>
                        </form>

                        <Link to='/reset-password' activeClassName="active" className="text-center">I forgot my password</Link>
                        <br />
                        <Link to='/register' activeClassName="active" className="text-center">Register a new membership</Link>
                    </div>
                </div>
            </BodyClassName>
        );
    }
});

module.exports = Login;
