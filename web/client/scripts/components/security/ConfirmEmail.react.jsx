var React = require('react');
var BodyClassName = require('react-body-classname');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;
var History = ReactRouter.History;
var request = require('superagent');
var NotificationSystem = require('react-notification-system');

var Constants = require('../../constants/Constants.js');
var APIEndpoints = Constants.APIEndpoints;

var AppForms = require('../form/AppForms.js');
var ConfirmEmailForm = require('../form/ConfirmEmailForm.js');
var SessionStore = require('../../stores/SessionStore.js');
var RegisterStore = require('../../stores/RegisterStore.js');
var UserActionCreators = require('../../actions/UserActionCreators.js');
var NotificationsMixin = require('../../mixin/NotificationsMixin.js');

var ResetPassword = React.createClass({
    mixins: [ History, NotificationsMixin ],
    getInitialState: function() {
        return {
            _csrf_token: null,
            form: new ConfirmEmailForm({
                controlled: true,
                onChange: this.forceUpdate.bind(this)
            })
        };
    },
    componentDidMount: function() {
        RegisterStore.addChangeListener(this._onChange);

        if (SessionStore.isLoggedIn()) {
            this.history.replaceState(null, `/`);
        }

        var query = this.props.location.query;
        if (query.hasOwnProperty('token')) {
            this.state.form.updateData({
                token: query.token
            });
        }

        request.get(APIEndpoints.GET_CSRF + '/confirm-email')
            .set('Accept', 'application/json')
            .withCredentials()
            .end(function(error, response) {
                this.state.form.updateData({
                    csrfToken: response.body.data.csrf_token
                });
            }.bind(this));
    },
    componentWillUnmount: function() {
        RegisterStore.removeChangeListener(this._onChange);
    },
    _onChange: function(event) {
        if (RegisterStore.hasErrors()) {
            this.addErrors(RegisterStore.getErrors());
        } else {
            this.history.replaceState(null, `/login`);
        }
    },
    _onSubmit: function(e) {
        e.preventDefault();

        var form = this.state.form;
        var isValid = form.validate();
        if (isValid) {
            UserActionCreators.confirmEmail(form.cleanedData);
        }
    },
    render: function() {
        return (
            <BodyClassName className="hold-transition register-page">
                <div className="register-box">
                    <NotificationSystem ref="notificationSystem" />
                    <div className="register-logo">
                        <b>Esti</b> Confirm Email
                    </div>

                    <div className="register-box-body">
                        <p className="login-box-msg">Enter confirmation token to confirm email</p>

                        <form onSubmit={this._onSubmit}>
                            {this.state.form.boundFields().map(AppForms.renderField)}

                            <div className="row">
                                <div className="col-xs-4 col-xs-offset-8">
                                    <button type="submit" className="btn btn-primary btn-block btn-flat">
                                        Confirm
                                    </button>
                                </div>
                            </div>
                        </form>

                        <Link to='/login' activeClassName="active" className="text-center">
                            I already have a membership
                        </Link>
                        <br />
                        <Link to='/register' activeClassName="active" className="text-center">
                            Register a new membership
                        </Link>
                        <br />
                        <Link to='/reset-password' activeClassName="active" className="text-center">
                            I forgot my password
                        </Link>
                    </div>
                </div>
            </BodyClassName>
        );
    }

});

module.exports = ResetPassword;

