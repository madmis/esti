var React = require('react');
var BodyClassName = require('react-body-classname');
var ReactRouter = require('react-router');
var History = ReactRouter.History;
var Link = ReactRouter.Link;
var request = require('superagent');
var NotificationSystem = require('react-notification-system');
var Loader = require('react-loader');

var Constants = require('../../constants/Constants.js');
var APIEndpoints = Constants.APIEndpoints;

var AppForms = require('../form/AppForms.js');
var RegisterForm = require('../form/RegisterForm.js');
var UserActionCreators = require('../../actions/UserActionCreators.js');
var SessionStore = require('../../stores/SessionStore.js');
var RegisterStore = require('../../stores/RegisterStore.js');
var NotificationsMixin = require('../../mixin/NotificationsMixin.js');


var Register = React.createClass({
    mixins: [ History, NotificationsMixin ],
    contextTypes: {
        router: React.PropTypes.func
    },
    getInitialState: function() {
        return {
            loaded: true,
            _csrf_token: null,
            form: new RegisterForm({
                controlled: true,
                onChange: this.forceUpdate.bind(this)
            })
        };
    },
    componentDidMount: function() {
        RegisterStore.addChangeListener(this._onChange);

        if (SessionStore.isLoggedIn()) {
            this.history.replaceState(null, `/`);
        }

        request.get(APIEndpoints.GET_CSRF + '/registration')
            .set('Accept', 'application/json')
            .withCredentials()
            .end(function(error, response) {
                this.state.form.updateData({
                    csrfToken: response.body.data.csrf_token
                });
            }.bind(this));
    },
    componentWillUnmount: function() {
        RegisterStore.removeChangeListener(this._onChange);
    },

    _onChange: function(event) {
        this.setState({ loaded: true });
        if (RegisterStore.hasErrors()) {
            this.addErrors(RegisterStore.getErrors());
        } else {
            this.history.replaceState(null, '/confirm-email');
        }
    },

    _onSubmit: function(e) {
        e.preventDefault();

        var form = this.state.form;
        var isValid = form.validate();
        if (isValid) {
            this.setState({ loaded: false });
            UserActionCreators.register(form.cleanedData);
        }
    },
    render: function() {
        return (
            <BodyClassName className="hold-transition register-page">
                <div className="register-box">
                    <Loader loaded={this.state.loaded} top="20px" left="50%" />
                    <NotificationSystem ref="notificationSystem" />
                    <div className="register-logo"><b>Esti</b> Register</div>

                    <div className="register-box-body">
                        <p className="login-box-msg">Register a new membership</p>

                        <form onSubmit={this._onSubmit}>
                            {this.state.form.boundFields().map(AppForms.renderField)}

                            <div className="row">
                                <div className="col-xs-4 col-xs-offset-8">
                                    <button type="submit" className="btn btn-primary btn-block btn-flat">Register
                                    </button>
                                </div>
                            </div>
                        </form>

                        <Link to='/login' activeClassName="active" className="text-center">
                            I already have a membership
                        </Link>
                        <br />
                        <Link to='/reset-password' activeClassName="active" className="text-center">
                            I forgot my password
                        </Link>
                    </div>
                </div>
            </BodyClassName>
        );
    }

});

module.exports = Register;
