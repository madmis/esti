var React = require('react');
var BodyClassName = require('react-body-classname');
var ReactRouter = require('react-router');
var History = ReactRouter.History;
var Link = ReactRouter.Link;
var Forms = require('newforms');
var NotificationSystem = require('react-notification-system');

var Constants = require('../../constants/Constants.js');
var Const = Constants.Const;

var AppForms = require('../form/AppForms.js');
var NotificationsMixin = require('../../mixin/NotificationsMixin.js');
var UserActionCreators = require('../../actions/UserActionCreators.js');
var ResetPasswordStore = require('../../stores/ResetPasswordStore.js');
var SessionStore = require('../../stores/SessionStore.js');

var ResetForm = Forms.Form.extend({
    email: Forms.EmailField({
        widgetAttrs: {
            'data-feedback': 'glyphicon-envelope',
            className: 'form-control',
            placeholder: "Email",
            autoComplete: "off"
        }
    })
});

var ResetPasswordRequest = React.createClass({
    mixins: [ History, NotificationsMixin ],
    getInitialState: function() {
        return {
            form: new ResetForm({
                controlled: true,
                onChange: this.forceUpdate.bind(this)
            })
        };
    },
    componentDidMount: function() {
        ResetPasswordStore.addChangeListener(this._onChange);

        if (SessionStore.isLoggedIn()) {
            this.history.replaceState(null, `/`);
        }
    },
    componentWillUnmount: function() {
        ResetPasswordStore.removeChangeListener(this._onChange);
    },
    _onChange: function(event) {
        if (ResetPasswordStore.hasErrors()) {
            this.addErrors(ResetPasswordStore.getErrors());
        } else {
            this.addNotification(
                ResetPasswordStore.getRequestMessage(),
                Const.NOTIFY_SUCCESS
            );
        }
    },
    _onSubmit: function(e) {
        e.preventDefault();

        var form = this.state.form;
        var isValid = form.validate();
        if (isValid) {
            UserActionCreators.resetPasswordRequest(form.cleanedData);
        }
    },
    render: function() {
        return (
            <BodyClassName className="hold-transition login-page">
                <div className="login-box">
                    <NotificationSystem ref="notificationSystem" />
                    <div className="login-logo">
                        <b>Esti</b> Reset Password
                    </div>

                    <div className="login-box-body">
                        <p className="login-box-msg">Enter your email to reset password</p>

                        <form onSubmit={this._onSubmit}>
                            {this.state.form.boundFields().map(AppForms.renderField)}

                            <div className="col-xs-6 col-xs-offset-6">
                                <button type="submit" className="btn btn-primary btn-block btn-flat">Reset Password</button>
                            </div>
                        </form>

                        <Link to='/login' activeClassName="active" className="text-center">I already have a membership</Link>
                        <br />
                        <Link to='/register' activeClassName="active" className="text-center">Register a new membership</Link>
                    </div>
                </div>
            </BodyClassName>
        );
    }

});

module.exports = ResetPasswordRequest;
