var React = require('react');
var ReactRouter = require('react-router');
var History = ReactRouter.History;

var UserActionCreators = require('../../actions/UserActionCreators.js');

var Logout = React.createClass({
    mixins: [ History ],
    componentDidMount() {
        UserActionCreators.logout();
        this.history.replaceState(null, `/login`);
    },

    render() {
        return <p>You are now logged out</p>
    }
});

module.exports = Logout;