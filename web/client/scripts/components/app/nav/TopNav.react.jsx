var React = require('react');
var ReactRouter = require('react-router');
var History = ReactRouter.History;
var Link = ReactRouter.Link;

var NavMessages = require('./NavMessages.react.jsx');
var NavUser = require('./NavUser.react.jsx');
var Config = require('../../../utils/Config.js');

var TopNav = React.createClass({
    getInitialState: function() {
        return {};
    },
    componentDidMount: function() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        return (
            <div className="navbar-custom-menu">
                <ul className="nav navbar-nav">
                    <NavMessages />
                    <NavUser />
                    <li>
                        <a data-toggle="control-sidebar"><i className="fa fa-gears"/></a>
                    </li>
                </ul>
            </div>
        );
    }

});

module.exports = TopNav;