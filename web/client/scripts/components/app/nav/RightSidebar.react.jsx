var React = require('react');
var ReactRouter = require('react-router');
var History = ReactRouter.History;
var Link = ReactRouter.Link;

var Config = require('../../../utils/Config.js');

var RightSidebar = React.createClass({
    getInitialState: function() {
        return {};
    },
    componentDidMount: function() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        return (
            <aside>
                <aside className="control-sidebar control-sidebar-dark">
                    <ul className="nav nav-tabs nav-justified control-sidebar-tabs">
                        <li>
                            <a href="#control-sidebar-home-tab" data-toggle="tab"><i className="fa fa-home"/></a>
                        </li>
                        <li>
                            <a href="#control-sidebar-settings-tab" data-toggle="tab"><i className="fa fa-gears"/></a>
                        </li>
                    </ul>
                    <div className="tab-content">
                        <div className="tab-pane" id="control-sidebar-home-tab">
                            <h3 className="control-sidebar-heading">Recent Activity</h3>
                            <ul className="control-sidebar-menu">
                                <li>
                                    <a>
                                        <i className="menu-icon fa fa-birthday-cake bg-red"/>
                                        <div className="menu-info">
                                            <h4 className="control-sidebar-subheading">Langdon's Birthday</h4>
                                            <p>Will be 23 on April 24th</p>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div className="tab-pane" id="control-sidebar-settings-tab">
                            <form method="post">
                                <h3 className="control-sidebar-heading">General Settings</h3>
                                <div className="form-group">
                                    <label className="control-sidebar-subheading">
                                        Report panel usage
                                        <input type="checkbox" className="pull-right" />
                                    </label>
                                    <p>Some information about this general settings option</p>
                                </div>
                            </form>
                        </div>
                    </div>
                </aside>
                <div className="control-sidebar-bg"></div>
            </aside>
        )
    }
});

module.exports = RightSidebar;