var React = require('react');
var ReactRouter = require('react-router');
var History = ReactRouter.History;
var Link = ReactRouter.Link;
var moment = require('moment');

var Config = require('../../../utils/Config.js');
var SessionStore = require('../../../stores/SessionStore.js');

var NavUser = React.createClass({
    getInitialState: function() {
        return {
            user: SessionStore.getUser()
        };
    },
    componentDidMount: function() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        var member = moment(this.state.user.created.date).format('MMM. YYYY');
        return (

            <li className="dropdown user user-menu">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                    <img src="http://placehold.it/160x160" className="user-image" alt="User Image" />
                        <span className="hidden-xs">{this.state.user.email}</span>
                </a>
                <ul className="dropdown-menu">
                    <li className="user-header">
                        <img src="http://placehold.it/160x160" className="img-circle" alt="User Image" />
                            <p>
                                {this.state.user.email}
                                <br/>Web Developer
                                <small>Member since {member}</small>
                            </p>
                    </li>
                    <li className="user-body">
                        <div className="col-xs-4 text-center">
                            <a href="#">Followers</a>
                        </div>
                        <div className="col-xs-4 text-center">
                            <a href="#">Sales</a>
                        </div>
                        <div className="col-xs-4 text-center">
                            <a href="#">Friends</a>
                        </div>
                    </li>
                    <li className="user-footer">
                        <div className="pull-left">
                            <Link to="/profile" className="btn btn-default btn-flat">Profile</Link>
                        </div>
                        <div className="pull-right">
                            <Link to="/logout" className="btn btn-default btn-flat">Logout</Link>
                        </div>
                    </li>
                </ul>
            </li>
        )
    }
});

module.exports = NavUser;
