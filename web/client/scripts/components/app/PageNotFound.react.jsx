var React = require('react');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;
var IndexLink = ReactRouter.IndexLink;
var BodyClassName = require('react-body-classname');

var PageNotFound = React.createClass({
    render: function() {
        return (
            <BodyClassName className="hold-transition login-page">
            <div>
                <section className="content-header">
                    <h1>404 Error Page</h1>
                    <ol className="breadcrumb">
                        <li>
                            <IndexLink to='/'><i className="fa fa-dashboard"></i> Home</IndexLink>
                        </li>
                        <li className="active">404 error</li>
                    </ol>
                </section>
                <section className="content">
                    <div className="error-page">
                        <h2 className="headline text-yellow"> 404</h2>

                        <div className="error-content">
                            <h3><i className="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

                            <p>
                                We could not find the page you were looking for.
                                Meanwhile, you may <IndexLink to='/'>return to home</IndexLink>.
                            </p>

                        </div>
                    </div>
                </section>
            </div>
            </BodyClassName>
        );
    }
});

module.exports = PageNotFound;