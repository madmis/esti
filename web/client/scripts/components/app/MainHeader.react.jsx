var React = require('react');
var ReactRouter = require('react-router');
var History = ReactRouter.History;
var Link = ReactRouter.Link;

var Config = require('../../utils/Config.js');
var TopNav = require('./nav/TopNav.react.jsx');

var MainHeader = React.createClass({
    getInitialState: function() {
        return {};
    },
    componentDidMount: function() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        return (
            <header className="main-header">
                <Link to='/' activeClassName="active" className="logo">
                    <span className="logo-mini"><b>{Config.app.name}</b></span>
                    <span className="logo-lg"><b>{Config.app.name}</b></span>
                </Link>
                <nav className="navbar navbar-static-top" role="navigation">
                    <a href="#" className="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span className="sr-only">Toggle navigation</span>
                    </a>
                    <TopNav />
                </nav>
            </header>
        );
    }

});

module.exports = MainHeader;