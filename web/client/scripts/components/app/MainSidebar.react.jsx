var React = require('react');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;
var IndexLink = ReactRouter.IndexLink;

var SessionStore = require('../../stores/SessionStore.js');

var MainSidebar = React.createClass({
    getInitialState: function() {
        return {
            user: SessionStore.getUser()
        };
    },
    render: function() {
        var userName = this.state.user.email.substring(
            0,
            this.state.user.email.indexOf('@')
        );
        return (
            <aside className="main-sidebar">
                <section className="sidebar">
                    <div className="user-panel">
                        <div className="pull-left image">
                            <img src="http://placehold.it/160x160" className="img-circle" alt="User Image" />
                        </div>
                        <div className="pull-left info">
                            <p>{userName}</p>
                            {/*<a href="#"><i className="fa fa-circle text-success"/> Online</a>*/}
                        </div>
                    </div>
                    <ul className="sidebar-menu">
                        <li className="header">MAIN NAVIGATION</li>
                        <li>
                            <IndexLink to='/'><i className="fa fa-home"/> Home</IndexLink>
                        </li>
                        <li>
                            <Link to='/profile' activeClassName="active"><i className="fa fa-user"/> Profile</Link>
                        </li>
                        <li className="treeview">
                            <a href="#">
                                <i className="fa fa-cubes"/> <span>Projects</span> <i className="fa fa-angle-left pull-right"/>
                            </a>
                            <ul className="treeview-menu">
                                <li>
                                    <Link to='/project' activeClassName="active"><i className="fa fa-list"/> Projects list</Link>
                                </li>
                                <li>
                                    <Link to='/project/create' activeClassName="active"><i className="fa fa-plus-square"/> Create project</Link>
                                </li>
                                <li>
                                    <Link to='/project/search' activeClassName="active"><i className="fa fa-search" /> Search projects</Link>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="../widgets.html">
                                <i className="fa fa-th"/> <span>Widgets</span>
                                <small className="label pull-right bg-green">new</small>
                            </a>
                        </li>
                        <li className="header">LABELS</li>
                        <li><a href="#"><i className="fa fa-circle-o text-red"/> <span>Important</span></a></li>
                        <li><a href="#"><i className="fa fa-circle-o text-yellow"/> <span>Warning</span></a></li>
                        <li><a href="#"><i className="fa fa-circle-o text-aqua"/> <span>Information</span></a></li>
                    </ul>
                </section>
            </aside>
        );
    }

});

module.exports = MainSidebar;
