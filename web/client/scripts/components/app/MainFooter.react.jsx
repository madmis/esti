var React = require('react');

var Config = require('../../utils/Config.js');

var MainFooter = React.createClass({
    getInitialState: function() {
        var date = new Date();
        return {
            year: date.getFullYear()
        };
    },
    render: function() {
        return (
            <footer className="main-footer">
                <div className="pull-right hidden-xs">
                    <b>Client Version</b> {Config.app.version}
                </div>
                <strong>
                    &copy; {this.state.year}
                    <a href={Config.app.host}> {Config.app.name}: {Config.app.slogan}</a>
                </strong>
            </footer>
        );
    }

});

module.exports = MainFooter;

