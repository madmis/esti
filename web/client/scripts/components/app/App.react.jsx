var React = require('react');
var BodyClassName = require('react-body-classname');

var MainHeader = require('./MainHeader.react.jsx');
var MainSidebar = require('./MainSidebar.react.jsx');
var MainFooter = require('./MainFooter.react.jsx');
var RightSidebar = require('./nav/RightSidebar.react.jsx');

var App = React.createClass({
    getInitialState: function() {
        return {};
    },

    componentDidMount: function() {
    },

    componentWillUnmount: function() {
    },

    render: function() {
        return (
            <BodyClassName className="skin-blue fixed">
                <div className="wrapper" id="main-wrapper">
                    <MainHeader/>
                    <MainSidebar/>
                    <div className="content-wrapper">
                        {this.props.children}
                    </div>
                    <MainFooter/>
                    <RightSidebar/>
                </div>
            </BodyClassName>
        );
    }

});

module.exports = App;

