var Forms = require('newforms');

var LoginForm = Forms.Form.extend({
    email: Forms.EmailField({
        widgetAttrs: {
            'data-feedback': 'glyphicon-envelope',
            className: 'form-control',
            placeholder: "Email",
            autoComplete: "off"
        }
    }),
    password: Forms.CharField({
        widget: Forms.PasswordInput,
        widgetAttrs: {
            'data-feedback': 'glyphicon-lock',
            className: 'form-control',
            placeholder: "Password",
            autoComplete: "off"
        }
    })
});

module.exports = LoginForm;