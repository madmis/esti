var Forms = require('newforms');

var ProjectForm = Forms.Form.extend({
    name: Forms.CharField({
        widgetAttrs: {
            'data-feedback': 'glyphicon-font',
            className: 'form-control',
            placeholder: "Project name"
        }
    }),
    description: Forms.CharField({
        widget: Forms.Textarea,
        widgetAttrs: {
            className: 'form-control',
            placeholder: "Project description",
            autoComplete: "off"
        }
    }),
    publicUri: Forms.URLField({
        widgetAttrs: {
            'data-feedback': 'glyphicon-link',
            className: 'form-control',
            placeholder: "Project public uri. Example: http://myproject.com"
        },
        required: false
    }),
    repository: Forms.CharField({
        widgetAttrs: {
            'data-feedback': 'glyphicon-link',
            className: 'form-control',
            placeholder: "Project public repository",
            autoComplete: "off"
        },
        required: false
    }),
    startDate: Forms.DateField({
        widgetAttrs: {
            'data-name': 'startDate',
            className: 'form-control datepicker',
            placeholder: 'Project start date'
        },
        widget: Forms.DateInput({format: '%m/%d/%Y'}),
        required: false
    }),
    endDate: Forms.DateField({
        widgetAttrs: {
            'data-name': 'endDate',
            className: 'form-control datepicker',
            placeholder: 'Project end date'
        },

        widget: Forms.DateInput({format: '%m/%d/%Y'}),
        required: false
    }),
    tags: Forms.CharField({
        label: 'Project tags',
        widgetAttrs: { ref: 'tagsInput' },
        widget: Forms.HiddenInput
    }),
    technologies: Forms.CharField({
        label: 'Project technologies',
        widgetAttrs: { ref: 'techsInput' },
        widget: Forms.HiddenInput
    }),
    clean: function() {
        var cleanedData = ProjectForm.__super__.clean.call(this);
        var startDate = this.cleanedData.startDate;
        var endDate = this.cleanedData.endDate;

        if (startDate && endDate && startDate >= endDate) {
            var message = "Start date should be less then end date.";
            this.addError('startDate', message);
        }
    }
});

module.exports = ProjectForm;