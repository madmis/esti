var Forms = require('newforms');

module.exports = {
    renderField: function(boundField) {
        var className = 'form-group';
        var feedback = '';
        var widgetAttrs = boundField.field.widgetAttrs;

        if (widgetAttrs.hasOwnProperty('data-feedback')) {
            className = className.concat(' has-feedback');
            feedback = React.DOM.span({
                className: 'glyphicon ' + widgetAttrs['data-feedback'] + ' form-control-feedback'
            });
        }

        if (boundField.status() == 'error') {
            className = className.concat(' has-error');
        }

        if (boundField.field instanceof Forms.BooleanField) {
            className = className.concat(' checkbox');
            return <div className={className}>
                <label>{boundField.render(widgetAttrs)} {boundField.label}</label>
                {boundField.helpTextTag()}
                <span className="help-block">
                    {boundField.errors().render()}
                </span>
            </div>
        } else {
            return <div className={className}>
                {boundField.render(widgetAttrs)}
                {boundField.helpTextTag()}
                <span className="help-block">
                    {boundField.errors().render()}
                </span>
                {feedback}
            </div>
        }
    }
};