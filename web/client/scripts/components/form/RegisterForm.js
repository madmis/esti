var Forms = require('newforms');

var RegisterForm = Forms.Form.extend({
    //name: Forms.CharField({
    //    widgetAttrs: {
    //        'data-feedback': 'glyphicon-user',
    //        className: 'form-control',
    //        placeholder: "Full name"
    //    }
    //}),
    email: Forms.EmailField({
        widgetAttrs: {
            'data-feedback': 'glyphicon-envelope',
            className: 'form-control',
            placeholder: "Email",
            autoComplete: "off"
        }
    }),
    password: Forms.CharField({
        widget: Forms.PasswordInput,
        widgetAttrs: {
            'data-feedback': 'glyphicon-lock',
            className: 'form-control',
            placeholder: "Password",
            autoComplete: "off"
        }
    }),
    confirmPassword: Forms.CharField({
        widget: Forms.PasswordInput,
        widgetAttrs: {
            'data-feedback': 'glyphicon-lock',
            className: 'form-control',
            placeholder: "Retype password",
            autoComplete: "off"
        }
    }),
    csrfToken: Forms.CharField({
        required: false,
        widgetAttrs: {
            hidden: true
        }
    }),
    acceptTerms: Forms.BooleanField({required: true})
});

module.exports = RegisterForm;