var Forms = require('newforms');

var ConfirmEmailForm = Forms.Form.extend({
    token: Forms.CharField({
        widgetAttrs: {
            'data-feedback': 'glyphicon-ok',
            className: 'form-control',
            placeholder: "Confirmation token",
            autoComplete: "off"
        }
    }),
    csrfToken: Forms.CharField({
        required: false,
        widgetAttrs: {
            hidden: true
        }
    })
});

module.exports = ConfirmEmailForm;
