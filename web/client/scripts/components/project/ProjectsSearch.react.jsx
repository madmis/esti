var React = require('react');
var ReactRouter = require('react-router');
var History = ReactRouter.History;
var Link = ReactRouter.Link;

var ProjectsSearch = React.createClass({
    render: function() {
        return (
            <div>
                <section className="content-header">
                    <h1>Search Projects</h1>
                    <ol className="breadcrumb">
                        <li><Link to='/'><i className="fa fa-dashboard"/> Home</Link></li>
                        <li className="active">search projects</li>
                    </ol>
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-md-3">
                        </div>

                        <div className="col-md-9"></div>
                    </div>
                </section>
            </div>
        );
    }
});

module.exports = ProjectsSearch;