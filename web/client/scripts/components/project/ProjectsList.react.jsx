var React = require('react');
var ReactRouter = require('react-router');
var History = ReactRouter.History;
var Link = ReactRouter.Link;
var request = require('superagent');
var Loader = require('react-loader');
var moment = require('moment');

var Constants = require('../../constants/Constants.js');
var APIEndpoints = Constants.APIEndpoints;
var SessionStore = require('../../stores/SessionStore.js');

var ProjectsList = React.createClass({
    getInitialState: function() {
        return {
            loaded: true,
            data: { projects: [] }
        };
    },
    componentDidMount: function() {
        this.setState({ loaded: false });
        request
            .createRequest('GET', APIEndpoints.PROJECTS_LIST)
            .end(function(error, response) {
                if (this.isMounted()) {
                    this.setState({
                        data: response.body.data,
                        loaded: true
                    });
                }
            }.bind(this));
    },
    render: function() {
        var projectsList;

        if (this.state.data.projects.length) {
            projectsList = this.state.data.projects.map(function(row, i) {
                return (
                    <div key={i} className="col-md-12">
                        <div className="box box-info">
                            <div className="box-header with-border">
                                <h2 className="box-title"><i className="fa fa-globe"/> {row.name}</h2>
                                <div className="box-tools pull-right">
                                    <div className="label bg-aqua">{moment(row.created).format('DD MMM. YYYY')}</div>
                                </div>
                            </div>
                            <div className="box-body">
                                <div className="row">
                                    <div className="col-md-6">
                                        <strong><i className="fa fa-book margin-r-5" /> Description:</strong>
                                        <p className="pad">{row.description}</p>
                                    </div>
                                    <div className="col-md-3">
                                        <strong><i className="fa fa-tag margin-r-5" /> Tags:</strong>
                                        <p>
                                            {row.tags.map(function(tag, ti) {
                                                return (
                                                    <button key={ti} className="btn btn-info btn-xs btn-flat margin">{tag.tag}</button>
                                                );
                                            })}
                                        </p>
                                    </div>
                                    <div className="col-md-3">
                                        <strong><i className="fa fa-pencil margin-r-5" /> Technologies:</strong>
                                        <p>
                                            {row.technologies.map(function(tech, ti) {
                                                return (
                                                    <button key={ti} className="btn bg-olive btn-xs btn-flat margin">{tech.name}</button>
                                                );
                                            })}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="box-footer">
                                    <span className="text-muted margin-r-5">
                                        <i className="fa fa-calendar margin-r-5"/>
                                        created: {moment(row.created).format('DD MMM. YYYY')}
                                    </span>
                                    <span className="text-muted margin-r-5">
                                        <i className="fa fa-calendar margin-r-5"/>
                                        updated: {moment(row.updated).format('DD MMM. YYYY')}
                                    </span>
                            </div>
                        </div>
                    </div>
                );
            });
        } else {
            projectsList = (
                <div className="description-block">
                    <span className="description-text text-muted">No projects</span>
                </div>
            );
        }

        return (
            <div>
                <Loader loaded={this.state.loaded} top="20px" left="50%" />
                <section className="content-header">
                    <h1>Projects</h1>
                    <ol className="breadcrumb">
                        <li><Link to='/'><i className="fa fa-dashboard"/> Home</Link></li>
                        <li className="active">projects</li>
                    </ol>
                </section>

                <section className="content">
                    {projectsList}
                </section>
            </div>
        );
    }
});

module.exports = ProjectsList;