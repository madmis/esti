var React = require('react');
var ReactRouter = require('react-router');
var History = ReactRouter.History;
var Link = ReactRouter.Link;
var NotificationSystem = require('react-notification-system');
var Loader = require('react-loader');
var moment = require('moment');
var classNames = require('classnames');
var assign = require('object-assign');
var Select = require('react-select');
var request = require('superagent');

var AppForms = require('../form/AppForms.js');
var NotificationsMixin = require('../../mixin/NotificationsMixin.js');
var ProjectFormForm = require('../form/ProjectForm.js');
var UserActionCreators = require('../../actions/UserActionCreators.js');
var Constants = require('../../constants/Constants.js');
var APIEndpoints = Constants.APIEndpoints;
var SessionStore = require('../../stores/SessionStore.js');
var ApiUtils = require('../../utils/WebAPIUtils.js');

var ProjectForm = React.createClass({
    mixins: [History, NotificationsMixin],
    getInitialState: function() {
        return {
            loaded: true,
            form: new ProjectFormForm({
                controlled: true,
                onChange: this.forceUpdate.bind(this),
                data: {
                    startDate: moment().toDate(),
                    endDate: moment().add(90, 'days').toDate()
                }
            }),
            tagsValue: [],
            techsValue: [],
            isLoadingTags: false,
            isLoadingTechs: false
        };
    },
    componentDidMount: function() {
        if (this.isMounted()) {
            jQuery('.datepicker').datepicker({
                autoclose: true,
                format: 'MM/DD/YYYY'.toLowerCase(),
                zIndexOffset: 1050
            }).on('changeDate', function(e) {
                var values = {};
                values[e.target.dataset.name] = e.date;
                this.state.form.updateData(values);
            }.bind(this));
        }
    },
    componentWillUnmount: function() {
    },
    _onSubmit: function(e) {
        e.preventDefault();

        var form = this.state.form;
        var isValid = form.validate();
        if (isValid) {
            this.setState({loaded: false});
            var data = form.cleanedData;
            var request = ApiUtils
                .createRequest('POST', APIEndpoints.PROJECT_CREATE)
                .withCredentials()
                .field('project[name]', data.name)
                .field('project[description]', data.description)
                .field('project[publicUri]', data.publicUri)
                .field('project[repository]', data.repository)
                .field('project[startDate]', data.startDate)
                .field('project[endDate]', data.endDate);

            data.tags.split(',').map(function(item) {
                request.field('tags[]', item)
            });
            data.technologies.split(',').map(function(item) {
                request.field('technologies[]', item)
            })

            request
                .end(function(error, response) {
                    console.debug(error, response);

                    if (error) {
                        this.addErrors(response.body.errors);
                    } else {
                        this.history.replaceState(null, '/project');
                    }
                    this.setState({loaded: true});
                }.bind(this));
        }
    },
    isFieldInvalid: function(fieldName) {
        return this.state.form.boundField(fieldName).status() == 'error';
    },
    handleTagsSelectChange: function(value) {
        var tags = value.map(function(item) {
            return item.value;
        }).join(',');

        this.setState({tagsValue: value});
        this.state.form.updateData({tags: tags});
    },
    getTagsOptions: function(input, callback) {
        this.setState({isLoadingTags: true});
        if (input.length > 2) {
            ApiUtils
                .createRequest('GET', APIEndpoints.DIC_SEARCH_TAGS)
                .query({term: input})
                .withCredentials()
                .end(function(error, response) {
                    var options = response.body.data.map(function(item) {
                        return {label: item.tag, value: item.tag};
                    });
                    callback(null, { options: options });
                    this.setState({isLoadingTags: false});
                }.bind(this));
        } else {
            this.setState({isLoadingTags: false});
            callback(null, {options: []});
        }
    },
    handleTechsSelectChange: function(value) {
        var techs = value.map(function(item) {
            return item.value;
        }).join(',');

        this.setState({techsValue: value});
        this.state.form.updateData({technologies: techs});
    },
    getTechsOptions: function(input, callback) {
        this.setState({isLoadingTechs: true});
        if (input.length > 2) {
            ApiUtils
                .createRequest('GET', APIEndpoints.DIC_SEARCH_TECH)
                .query({term: input})
                .withCredentials()
                .end(function(error, response) {
                    var options = response.body.data.map(function(item) {
                        return {label: item.name, value: item.name};
                    });
                    callback(null, { options: options });
                    this.setState({isLoadingTechs: false});
                }.bind(this));
        } else {
            this.setState({isLoadingTechs: false});
            callback(null, {options: []});
        }
    },
    render: function() {
        var formGroupClass = {'form-group': true};
        var nameClass = classNames(assign({}, formGroupClass, {
            'col-md-12': true,
            'has-error': this.isFieldInvalid('name')
        }));
        var descriptionClass = classNames(assign({}, formGroupClass, {
            'col-md-12': true,
            'has-error': this.isFieldInvalid('description')
        }));
        var publicUriClass = classNames(assign({}, formGroupClass, {
            'col-md-6': true,
            'has-error': this.isFieldInvalid('publicUri')
        }));
        var repositoryClass = classNames(assign({}, formGroupClass, {
            'col-md-6': true,
            'has-error': this.isFieldInvalid('repository')
        }));
        var startDateClass = classNames(assign({}, formGroupClass, {
            'col-md-6': true,
            'has-error': this.isFieldInvalid('startDate')
        }));
        var endDateClass = classNames(assign({}, formGroupClass, {
            'col-md-6': true,
            'has-error': this.isFieldInvalid('endDate')
        }));
        var tagsClass = classNames(assign({}, formGroupClass, {
            'col-md-12': true,
            'has-error': this.isFieldInvalid('tags')
        }));
        var techsClass = classNames(assign({}, formGroupClass, {
            'col-md-12': true,
            'has-error': this.isFieldInvalid('technologies')
        }));

        return (
            <div>
                <Loader loaded={this.state.loaded} top="20px" left="50%"/>
                <NotificationSystem ref="notificationSystem"/>

                <section className="content-header">
                    <h1>Create Project</h1>
                    <ol className="breadcrumb">
                        <li><Link to='/'><i className="fa fa-dashboard"/> Home</Link></li>
                        <li className="active">new project</li>
                    </ol>
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-md-12">
                            <form onSubmit={this._onSubmit}>
                                <div className="row">
                                    <div className={nameClass}>
                                        <label>{this.state.form.boundField('name').labelTag()}</label>
                                        {this.state.form.boundField('name').render()}
                                        <span className="help-block">
                                        {this.state.form.boundField('name').errors().render()}
                                        </span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className={descriptionClass}>
                                        <label>{this.state.form.boundField('description').labelTag()}</label>
                                        {this.state.form.boundField('description').render()}
                                        <span className="help-block">
                                        {this.state.form.boundField('description').errors().render()}
                                        </span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className={tagsClass}>
                                        {this.state.form.boundField('tags').render()}
                                        <label>{this.state.form.boundField('tags').labelTag()}</label>
                                        <Select.Async
                                            autoload={false}
                                            cache={false}
                                            minimumInput={2}
                                            isLoading={this.state.isLoadingTags}
                                            loadOptions={this.getTagsOptions}
                                            className="form-control"
                                            multi={true}
                                            allowCreate={true}
                                            value={this.state.tagsValue}
                                            placeholder="Select tags (type 2 characters to start searching)"
                                            onChange={this.handleTagsSelectChange}
                                        />
                                        <span className="help-block">
                                            {this.state.form.boundField('tags').errors().render()}
                                        </span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className={techsClass}>
                                        {this.state.form.boundField('technologies').render()}
                                        <label>{this.state.form.boundField('technologies').labelTag()}</label>
                                        <Select.Async
                                            autoload={false}
                                            cache={false}
                                            minimumInput={2}
                                            isLoading={this.state.isLoadingTechs}
                                            loadOptions={this.getTechsOptions}
                                            className="form-control"
                                            multi={true}
                                            allowCreate={true}
                                            value={this.state.techsValue}
                                            placeholder="Select technologies (type 2 characters to start searching)"
                                            onChange={this.handleTechsSelectChange}
                                        />
                                        <span className="help-block">
                                            {this.state.form.boundField('technologies').errors().render()}
                                        </span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className={publicUriClass}>
                                        <label>{this.state.form.boundField('publicUri').labelTag()}</label>
                                        {this.state.form.boundField('publicUri').render()}
                                        <span className="help-block">
                                        {this.state.form.boundField('publicUri').errors().render()}
                                        </span>
                                    </div>
                                    <div className={repositoryClass}>
                                        <label>{this.state.form.boundField('repository').labelTag()}</label>
                                        {this.state.form.boundField('repository').render()}
                                        <span className="help-block">
                                        {this.state.form.boundField('repository').errors().render()}
                                        </span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className={startDateClass}>
                                        <label>{this.state.form.boundField('startDate').labelTag()}</label>
                                        {this.state.form.boundField('startDate').render()}
                                        <span className="help-block">
                                        {this.state.form.boundField('startDate').errors().render()}
                                        </span>
                                    </div>
                                    <div className={endDateClass}>
                                        <label>{this.state.form.boundField('endDate').labelTag()}</label>
                                        {this.state.form.boundField('endDate').render()}
                                        <span className="help-block">
                                        {this.state.form.boundField('endDate').errors().render()}
                                        </span>
                                    </div>
                                </div>

                                <div className="row form-group">
                                    <div className="col-xs-4 col-xs-offset-8">
                                        <button type="submit" className="btn btn-primary btn-block btn-flat">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
        );
    }
});

module.exports = ProjectForm;