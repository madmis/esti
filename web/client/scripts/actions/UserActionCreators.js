var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var Constants = require('../constants/Constants.js');
var WebAPIUtils = require('../utils/WebAPIUtils.js');

var ActionTypes = Constants.ActionTypes;

module.exports = {
    login: function(email, password) {
        AppDispatcher.handleViewAction({
            type: ActionTypes.LOGIN_REQUEST,
            email: email,
            password: password
        });
        WebAPIUtils.login(email, password);
    },

    /**
     * {email: "", password: "", confirmPassword: "", csrfToken: "", acceptTerms: bool}
     * @param {object} data
     */
    register: function(data) {
        AppDispatcher.handleViewAction({
            type: ActionTypes.REGISTER_REQUEST,
            data: data
        });
        WebAPIUtils.register(data);
    },
    /**
     * {token: "", csrfToken: ""}
     * @param {object} data
     */
    confirmEmail: function(data) {
        AppDispatcher.handleViewAction({
            type: ActionTypes.CONFIRM_EMAIL_REQUEST,
            data: data
        });
        WebAPIUtils.confirmEmail(data);
    },

    logout: function() {
        AppDispatcher.handleViewAction({
            type: ActionTypes.LOGOUT
        });
    },

    /**
     * {email: ""}
     * @param {object} data
     */
    resetPasswordRequest: function(data) {
        AppDispatcher.handleViewAction({
            type: ActionTypes.RESET_PASSWORD_REQUEST_REQUEST,
            data: data
        });
        WebAPIUtils.resetPasswordRequest(data);
    },
    /**
     * {password: "", confirmPassword: "", csrfToken: ""}
     * @param {object} data
     */
    resetPassword: function(data) {
        AppDispatcher.handleViewAction({
            type: ActionTypes.RESET_PASSWORD_REQUEST,
            data: data
        });
        WebAPIUtils.resetPassword(data);
    }
};