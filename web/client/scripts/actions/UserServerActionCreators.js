var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var Constants = require('../constants/Constants.js');

var ActionTypes = Constants.ActionTypes;

module.exports = {
    receiveLogin: function(response, errors) {
        AppDispatcher.handleServerAction({
            type: ActionTypes.LOGIN_RESPONSE,
            response: response,
            errors: errors
        });
    },
    receiveRegister: function(response, errors) {
        AppDispatcher.handleServerAction({
            type: ActionTypes.REGISTER_RESPONSE,
            response: response,
            errors: errors
        });
    },
    receiveConfirmEmail: function(response, errors) {
        AppDispatcher.handleServerAction({
            type: ActionTypes.CONFIRM_EMAIL_RESPONSE,
            response: response,
            errors: errors
        });
    },
    receiveResetPasswordRequest: function(response, errors) {
        AppDispatcher.handleServerAction({
            type: ActionTypes.RESET_PASSWORD_REQUEST_RESPONSE,
            response: response,
            errors: errors
        });
    },
    receiveResetPassword: function(response, errors) {
        AppDispatcher.handleServerAction({
            type: ActionTypes.RESET_PASSWORD_RESPONSE,
            response: response,
            errors: errors
        });
    }
};