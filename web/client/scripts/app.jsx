var React = require('react');
var ReactDOM = require('react-dom');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var IndexRoute = ReactRouter.IndexRoute;

var createHistory = require('history').createHistory;
var history = createHistory();

window.React = React;

var App = require('./components/app/App.react.jsx');
var Home = require('./components/Home.react.jsx');
var Profile = require('./components/Profile.react.jsx');
var PageNotFound = require('./components/app/PageNotFound.react.jsx');
var Login = require('./components/security/Login.react.jsx');
var Register = require('./components/security/Register.react.jsx');
var ResetPassword = require('./components/security/ResetPassword.react.jsx');
var ResetPasswordRequest = require('./components/security/ResetPasswordRequest.react.jsx');
var ConfirmEmail = require('./components/security/ConfirmEmail.react.jsx');
var Logout = require('./components/security/Logout.react.jsx');
var ProjectsList = require('./components/project/ProjectsList.react.jsx');
var ProjectForm = require('./components/project/ProjectForm.react.jsx');
var ProjectsSearch = require('./components/project/ProjectsSearch.react.jsx');

var SessionStore = require('./stores/SessionStore.js');

function requireAuth(nextState, replaceState) {
    if (!SessionStore.isLoggedIn()) {
        replaceState({ nextPathname: nextState.location.pathname }, '/login');
    }
}

ReactDOM.render(
    <Router history={history}>
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="/confirm-email" component={ConfirmEmail} />
        <Route path="/reset-password/:token" component={ResetPassword} />
        <Route path="/reset-password" component={ResetPasswordRequest} />
        <Route path="/" component={App}>
            <IndexRoute component={Home} onEnter={requireAuth} />
            <Route path='/logout' component={Logout} onEnter={requireAuth} />
            <Route path='/profile' component={Profile} onEnter={requireAuth} />
            <Route path='/project' component={ProjectsList} onEnter={requireAuth} />
            <Route path='/project/create' component={ProjectForm} onEnter={requireAuth} />
            <Route path='/project/search' component={ProjectsSearch} onEnter={requireAuth} />
        </Route>
        <Route path="*" component={PageNotFound} />
    </Router>,
    document.getElementById('main')
);
