'use strict';

var Const = require('../constants/Constants.js').Const;

var NotificationsMixin = {
    _notificationSystem: null,

    componentDidMount: function() {
        this._notificationSystem = this.refs.notificationSystem;
    },
    /**
     * @param {string} message
     * @param {string} level
     * @param {string} position
     */
    addNotification: function(message, level, position) {
        var options = {
            message: message,
            level: level ? level : Const.NOTIFY_ERROR,
            position: position ? position : Const.NOTIFY_TC,
            autoDismiss: false
        };
        this._notificationSystem.addNotification(options);
    },
    /**
     * @param {array} errors
     */
    addErrors: function(errors) {
        errors.forEach(function(item) {
            this.addNotification(item.message);
        }.bind(this));
    },
    getNotificationSystem: function() {
        return this._notificationSystem;
    }
};

module.exports = NotificationsMixin;