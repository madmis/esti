var keyMirror = require('keymirror');

var APIRoot = "http://esti.dev";

module.exports = {
    APIEndpoints: {
        LOGIN: APIRoot + "/api/login_check",
        REGISTER: APIRoot + "/en/api/register",
        GET_CSRF: APIRoot + "/en/api/get-csrf",
        CONFIRM_EMAIL: APIRoot + "/en/api/confirm-email",
        RESET_PASSWORD_REQUEST: APIRoot + "/en/api/reset-password",
        RESET_PASSWORD: APIRoot + "/en/api/reset-password/{{token}}",
        PROJECTS_LIST: APIRoot + "/en/api/project",
        PROJECT_CREATE: APIRoot + "/en/api/project",

        DIC_SEARCH_TAGS: APIRoot + "/en/api/dictionary/tag/search",
        DIC_SEARCH_TECH: APIRoot + "/en/api/dictionary/technology/search"
    },
    Const: {
        //notify
        NOTIFY_ERROR: 'error',
        NOTIFY_INFO: 'info',
        NOTIFY_SUCCESS: 'success',
        NOTIFY_WARNING: 'warning',
        // positions
        NOTIFY_TC: 'tc',
        NOTIFY_TR: 'tr'
    },

    PayloadSources: keyMirror({
        SERVER_ACTION: null,
        VIEW_ACTION: null
    }),

    ActionTypes: keyMirror({
        // Session
        LOGIN_REQUEST: null,
        LOGIN_RESPONSE: null,
        REGISTER_RESPONSE: null,
        REGISTER_REQUEST: null,
        CONFIRM_EMAIL_REQUEST: null,
        CONFIRM_EMAIL_RESPONSE: null,
        LOGOUT: null,
        RESET_PASSWORD_REQUEST_REQUEST: null,
        RESET_PASSWORD_REQUEST_RESPONSE: null,
        RESET_PASSWORD_REQUEST: null,
        RESET_PASSWORD_RESPONSE: null,

        // Routes
        REDIRECT: null
    })
};
