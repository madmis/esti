var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var Constants = require('../constants/Constants.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var ActionTypes = Constants.ActionTypes;
var CHANGE_EVENT = 'change';

// Load an access token from the session storage, you might want to implement
// a 'remember me' using localSgorage
var _accessToken = sessionStorage.getItem('accessToken');
var _user = sessionStorage.getItem('user') ? JSON.parse(sessionStorage.getItem('user')) : null;
var _errors = [];

var SessionStore = assign({}, EventEmitter.prototype, {

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    isLoggedIn: function() {
        return _accessToken ? true : false;
    },

    getAccessToken: function() {
        return _accessToken;
    },

    getUser: function() {
        return _user;
    },

    getErrors: function() {
        return _errors;
    }
});

SessionStore.dispatchToken = AppDispatcher.register(function(payload) {
    var action = payload.action;

    switch (action.type) {

        case ActionTypes.LOGIN_RESPONSE:
            if (action.response && action.response.token) {
                _accessToken = action.response.token;
                _user = action.response.user;
                // Token will always live in the session, so that the API can grab it with no hassle
                sessionStorage.setItem('accessToken', _accessToken);
                sessionStorage.setItem('user', JSON.stringify(_user));
            }

            if (action.errors) {
                _errors = action.errors;
            }

            SessionStore.emitChange();
            break;

        case ActionTypes.LOGOUT:
            _accessToken = null;
            _user = null;
            sessionStorage.removeItem('accessToken');
            sessionStorage.removeItem('user');
            SessionStore.emitChange();
            break;

        default:
    }

    return true;
});

module.exports = SessionStore;

