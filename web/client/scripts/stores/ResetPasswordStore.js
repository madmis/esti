var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var Constants = require('../constants/Constants.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var ActionTypes = Constants.ActionTypes;
var CHANGE_EVENT = 'change';

var _errors = [];
var _requestMessage = '';
var _resetMessage = '';

var ResetPasswordStore = assign({}, EventEmitter.prototype, {

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    getErrors: function() {
        var err = _errors;
        _errors = [];
        return err;
    },
    hasErrors: function() {
        return (_errors.length > 0)
    },
    getRequestMessage: function() {
        return _requestMessage;
    },
    getResetMessage: function() {
        return _resetMessage;
    }
});

ResetPasswordStore.dispatch = AppDispatcher.register(function(payload) {
    var action = payload.action;

    switch (action.type) {

        case ActionTypes.RESET_PASSWORD_REQUEST_RESPONSE:
            if (action.errors) {
                _errors = action.errors;
            } else {
                if (action.response.message) {
                    _requestMessage = action.response.message;
                }
            }

            ResetPasswordStore.emitChange();
            break;
        case ActionTypes.RESET_PASSWORD_RESPONSE:
            if (action.errors) {
                _errors = action.errors;
            } else {
                if (action.response.message) {
                    _resetMessage = action.response.message;
                }
            }

            ResetPasswordStore.emitChange();
            break;

        default:
    }

    return true;
});

module.exports = ResetPasswordStore;
