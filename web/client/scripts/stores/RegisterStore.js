var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var Constants = require('../constants/Constants.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var ActionTypes = Constants.ActionTypes;
var CHANGE_EVENT = 'change';

var _errors = [];

var RegisterStore = assign({}, EventEmitter.prototype, {

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    getErrors: function() {
        var err = _errors;
        _errors = [];
        return err;
    },
    hasErrors: function() {
        return (_errors.length > 0)
    }

});

RegisterStore.dispatch = AppDispatcher.register(function(payload) {
    var action = payload.action;

    switch (action.type) {

        case ActionTypes.REGISTER_RESPONSE:
            if (action.errors) {
                _errors = action.errors;
            }

            RegisterStore.emitChange();
            break;
        case ActionTypes.CONFIRM_EMAIL_RESPONSE:
            if (action.errors) {
                _errors = action.errors;
            }

            RegisterStore.emitChange();
            break;

        default:
    }

    return true;
});

module.exports = RegisterStore;
