module.exports = {
    app: {
        host: 'http://app.esti.dev/',
        name: 'Esti',
        slogan: 'Estimate your projects easy',
        version: '0.0.1'
    }
};
