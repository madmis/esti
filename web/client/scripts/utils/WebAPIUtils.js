var request = require('superagent');

var UserServerActionCreators = require('../actions/UserServerActionCreators.js');
var Constants = require('../constants/Constants.js');
var SessionStore = require('../stores/SessionStore.js');

var APIEndpoints = Constants.APIEndpoints;

/**
 * Render template
 * @param {String} template
 * @param {Object} data
 * @returns {String}
 */
var _replace = function (template, data) {
    for (var key in data) {
        if (!data.hasOwnProperty(key)) {
            continue;
        }
        var value = null == data[key] ? '' : (data[key].call ? data[key]() : data[key]);
        template = template.replace(new RegExp('\{\{' + key + '\}\}', 'g'), value.toString());
    }

    return template;
};

module.exports = {
    createRequest: function(method, url) {
        return request(method, url)
            .set('Accept', 'application/json')
            .set('X-Requested-With', 'XMLHttpRequest')
            .set('Authorization', 'Bearer ' + SessionStore.getAccessToken());
    },

    login: function(email, password) {
        request.post(APIEndpoints.LOGIN)
            .type('form')
            .send({_username: email, _password: password, grant_type: 'password'})
            .set('Accept', 'application/json')
            .end(function(error, response) {
                if (error) {
                    UserServerActionCreators.receiveLogin(null, response.body.errors);
                } else {
                    UserServerActionCreators.receiveLogin(response.body.data, null);
                }
            });
    },
    register: function(data) {
        request.post(APIEndpoints.REGISTER)
            .field('user_registration[email]', data.email)
            .field('user_registration[plainPassword][first]', data.password)
            .field('user_registration[plainPassword][second]', data.confirmPassword)
            .field('user_registration[_token]', data.csrfToken)
            .set('Accept', 'application/json')
            .end(function(error, response) {
                if (error) {
                    UserServerActionCreators.receiveRegister(null, response.body.errors);
                } else {
                    UserServerActionCreators.receiveRegister(response.body.data, null);
                }
            });
    },
    confirmEmail: function(data) {
        request.post(APIEndpoints.CONFIRM_EMAIL)
            .field('token', data.token)
            .field('_token', data.csrfToken)
            .set('Accept', 'application/json')
            .end(function(error, response) {
                if (error) {
                    UserServerActionCreators.receiveConfirmEmail(null, response.body.errors);
                } else {
                    UserServerActionCreators.receiveConfirmEmail(response.body.data, null);
                }
            });
    },
    resetPasswordRequest: function(data) {
        request.post(APIEndpoints.RESET_PASSWORD_REQUEST)
            .field('email', data.email)
            .set('Accept', 'application/json')
            .end(function(error, response) {
                if (error) {
                    UserServerActionCreators
                        .receiveResetPasswordRequest(null, response.body.errors);
                } else {
                    UserServerActionCreators
                        .receiveResetPasswordRequest(response.body.data, null);
                }
            });
    },
    resetPassword: function(data) {
        var uri = _replace(
            APIEndpoints.RESET_PASSWORD,
            {token: data.token}
        );

        request.post(uri)
            //.withCredentials()
            .field('password', data.password)
            .field('confirmPassword', data.confirmPassword)
            .field('_token', data.csrfToken)
            .set('Accept', 'application/json')
            .end(function(error, response) {
                if (error) {
                    UserServerActionCreators
                        .receiveResetPassword(null, response.body.errors);
                } else {
                    UserServerActionCreators
                        .receiveResetPassword(response.body.data, null);
                }
            });
    }
};

