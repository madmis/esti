'use strict';

var gulp = require('gulp'),
    p = {
        jsx: './scripts/app.jsx',
        scss: 'styles/main.scss',
        scssSource: 'styles/*',
        font: 'fonts/*',
        bundle: 'bundle.js',
        distJs: 'dist/js',
        distCss: 'dist/css',
        distFont: 'dist/fonts',
        distImages: 'dist/images'
    };

// Vendor scripts
gulp.task('vendor', function() {
    var concat = require('gulp-concat');

    gulp.src([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/modernizr/modernizr.js',
        'bower_components/adminlte/dist/js/app.min.js',
        'bower_components/adminlte/plugins/slimScroll/jquery.slimscroll.js',
        'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'
    ])
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(p.distJs));

    gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.min.css',
        'bower_components/font-awesome/css/font-awesome.min.css',
        'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.standalone.min.css'
    ])
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest(p.distCss));

    gulp.src([
        'bower_components/adminlte/dist/css/AdminLTE.min.css',
        'bower_components/adminlte/dist/css/skins/skin-blue.min.css'
    ])
        .pipe(concat('theme.css'))
        .pipe(gulp.dest(p.distCss));
});

gulp.task('fonts', function () {
    return gulp.src([
        'bower_components/bootstrap/dist/fonts/**',
        'bower_components/font-awesome/fonts/**'
    ])
        .pipe(gulp.dest(p.distFont))
});

gulp.task('images', function () {
    return gulp.src('images/**/*')
        .pipe(gulp.dest(p.distImages));
});

gulp.task('clean', function (cb) {
    var del = require('del');
    return del(['dist'], cb);
});

// Scripts
gulp.task('scripts', function() {
    var browserify = require('gulp-browserify'),
        reactify = require('reactify'),
        rename = require('gulp-rename'),
        gutil = require('gulp-util');

    return gulp.src('scripts/app.jsx')
        .pipe(browserify({
            debug: true,
            extensions: ['.jsx', '.js', '.json'],
            transform: [reactify]
        }))
        .on('error', function(err) {
            gutil.log(err.message)
        })
        .pipe(rename('bundle.js'))
        .pipe(gulp.dest(p.distJs))
});

gulp.task('styles', function () {
    var sass = require('gulp-sass');

    return gulp.src(p.scss)
        .pipe(sass({errLogToConsole: true}))
        .pipe(gulp.dest(p.distCss));
});

// Watch
gulp.task('watch', ['build'], function() {
    gulp.watch('scripts/**', ['scripts']);
    gulp.watch('styles/**', ['styles']);
});

gulp.task('build', ['clean'], function () {
    //process.env.NODE_ENV = 'production';
    gulp.start(['vendor', 'fonts', 'images', 'scripts', 'styles']);
});

gulp.task('default', function () {
    console.log('Run "gulp watch or gulp build"');
});