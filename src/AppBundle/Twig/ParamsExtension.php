<?php

namespace AppBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ParamsExtension
 * @package AppBundle\Twig
 */
class ParamsExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('param', [$this, 'getConfigParameterFilter']),
        ];
    }

    /**
     * @param string $name parameter key/name
     * @param mixed $default
     * @return mixed
     */
    public function getConfigParameterFilter($name, $default = null)
    {
        if (!$this->container->hasParameter($name)) {
            return $default;
        }

        return $this->container->getParameter($name);
    }


    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'app_params_extension';
    }
}