<?php

namespace AppBundle\Event\Listener;

use AppBundle\Entity\EntityToArrayInterface;
use AppBundle\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ApiAuthenticationListener
 * @package AppBundle\Event\Listener
 */
class ApiAuthenticationListener
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
        $response = $event->getResponse();
        $ex = $event->getException();
        $statusCode = $response->getStatusCode();
        $response = $this->container->get('http.response_formatter')
            ->addError($ex->getMessage(), $response->getStatusCode())
            ->jsonResponse($statusCode, $response->headers->all());

        // event doesn't allow set response ($event->setResponse())
        // that's why we replace response in such case
        $event->getResponse()->setContent($response->getContent());
    }

    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();
        $response = $event->getResponse();

        if (!$user instanceof User) {
            return;
        }

        // $data['token'] contains the JWT
        if ($user instanceof EntityToArrayInterface) {
            $data['user'] = $user->toArray();
        } else {
            $data['user'] = [
                'email' => $user->getEmail(),
                'roles' => $user->getRoles(),
            ];
        }

        $response = $this->container->get('http.response_formatter')
            ->setSuccess($data)
            ->jsonResponse($response->getStatusCode());

        $data = json_decode($response->getContent(), true);
        // event doesn't allow set response ($event->setResponse())
        // that's why we replace response in such case
        $event->setData($data);
    }
}