<?php

namespace AppBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Class AppExtension
 * @package AppBundle\DependencyInjection
 */
class AppExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator([
            __DIR__ . '/../Resources/config',
            __DIR__ . '/../Resources/config/services',
        ]));
        $loader->load('services.yml');
        $loader->load('repository.yml');
        $loader->load('manager.yml');
        $loader->load('mailer.yml');
        $loader->load('listener.yml');
        $loader->load('helper.yml');
        $loader->load('client_app_urls.yml');

    }
}
