<?php


namespace AppBundle\Entity;

/**
 * Class EntityToArrayInterface
 * @package AppBundle\Entity
 */
interface EntityToArrayInterface
{
    /**
     * @return array
     */
    public function toArray();
}