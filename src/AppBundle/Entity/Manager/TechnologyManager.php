<?php

namespace AppBundle\Entity\Manager;

use AppBundle\Entity\Technology;

/**
 * Class TechnologyManager
 * @package AppBundle\Entity\Manager
 * @method \AppBundle\Entity\Repository\TechnologyRepository repo()
 */
class TechnologyManager extends AbstractEntityManager
{
    /**
     * @param string $name
     * @return Technology
     */
    public function create($name)
    {
        $tech = new Technology();
        $tech->setName($name);

        $this->em()->persist($tech);
        $this->em()->flush();

        return $tech;
    }
}