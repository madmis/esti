<?php

namespace AppBundle\Entity\Manager;

use AppBundle\Entity\Tag;

/**
 * Class ProjectTagManager
 * @package AppBundle\Entity\Manager
 * @method \AppBundle\Entity\Repository\ProjectTagRepository repo()
 */
class ProjectTagManager extends AbstractEntityManager
{
    /**
     * @param string $tagName
     * @return Tag
     */
    public function create($tagName)
    {
        $tag = new Tag();
        $tag->setTag($tagName);

        $this->em()->persist($tag);
        $this->em()->flush();

        return $tag;
    }
}