<?php

namespace AppBundle\Entity\Manager;

use AppBundle\Entity\Repository\AbstractEntityRepository;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;

/**
 * Class AbstractEntityManager
 * @package AppBundle\Entity\Manager
 */
abstract class AbstractEntityManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var AbstractEntityRepository
     */
    private $repository;

    /**
     * @param EntityManager $em
     * @param ObjectRepository $repository
     */
    public function __construct(EntityManager $em, ObjectRepository $repository)
    {
        $this->em = $em;
        $this->repository = $repository;
    }

    /**
     * @return EntityManager
     */
    public function em()
    {
        return $this->em;
    }

    /**
     * @return AbstractEntityRepository
     */
    public function repo()
    {
        return $this->repository;
    }
}