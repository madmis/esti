<?php

namespace AppBundle\Entity\Manager;

use AppBundle\Entity\ConfirmationToken;
use AppBundle\Entity\User;

/**
 * Class ConfirmationTokenManager
 * @package AppBundle\Entity\Manager
 * @method \AppBundle\Entity\Repository\ConfirmationTokenRepository repo()
 */
class ConfirmationTokenManager extends AbstractEntityManager
{
    /**
     * @param User $user
     *
     * @return ConfirmationToken
     */
    public function createToken(User $user)
    {
        $token = new ConfirmationToken();
        $token->setToken(
            md5(uniqid($user->getEmail(), true))
        );
        $token->setUser($user);

        return $token;
    }

    /**
     * @param ConfirmationToken $token
     */
    public function removeToken(ConfirmationToken $token)
    {
        $this->em()->remove($token);
        $this->em()->flush();
    }

    /**
     * @param string $token
     * @return null|ConfirmationToken
     */
    public function findToken($token)
    {
        return $this->repo()->findOneBy([
            'token' => $token
        ]);
    }

    /**
     * @param User $user
     * @return int num of deleted rows
     */
    public function deleteUserTokens(User $user)
    {
        $q = $this->em()->createQuery(
            'delete from AppBundle\Entity\ConfirmationToken ct where ct.user = :user'
        );
        $numDeleted = $q->execute(['user' => $user]);

        return $numDeleted;
    }
}