<?php

namespace AppBundle\Entity\Manager;

use AppBundle\Entity\ResetPasswordToken;
use AppBundle\Entity\User;

/**
 * Class ResetPasswordTokenManager
 * @package AppBundle\Entity\Manager
 * @method \AppBundle\Entity\Repository\ResetPasswordTokenRepository repo()
 */
class ResetPasswordTokenManager extends AbstractEntityManager
{
    /**
     * @param User $user
     * @param int $tokenTTL token time to live in hours
     *
     * @return ResetPasswordToken
     */
    public function createToken(User $user, $tokenTTL)
    {
        if (!$tokenTTL) {
            $tokenTTL = 1;
        }

        $token = new ResetPasswordToken();
        $token->setToken(
            md5(uniqid($user->getEmail(), true))
        );
        $token->setExpires(
            (new \DateTime())->add(new \DateInterval("PT{$tokenTTL}H"))
        );
        $token->setUser($user);

        return $token;
    }

    /**
     * @param ResetPasswordToken $token
     */
    public function removeToken(ResetPasswordToken $token)
    {
        $this->em()->remove($token);
        $this->em()->flush();
    }
}