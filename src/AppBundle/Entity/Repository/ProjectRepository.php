<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Project;
use AppBundle\Entity\User;

/**
 * Class ProjectRepository
 * @package AppBundle\Entity\Repository
 */
class ProjectRepository extends AbstractEntityRepository
{
    /**
     * @param User $user
     * @return array|Project[]
     */
    public function getUserProjects(User $user)
    {
        return $this->findBy(['user' => $user]);
    }
}