<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\ConfirmationToken;

/**
 * Class ConfirmationTokenRepository
 * @package AppBundle\Entity\Repository
 */
class ConfirmationTokenRepository extends AbstractEntityRepository
{
    /**
     * @param $token
     * @return null|ConfirmationToken
     */
    public function findToken($token)
    {
        return $this->findOneBy([
            'token' => $token,
        ]);
    }

    /**
     * @param $userId
     * @return null|ConfirmationToken
     */
    public function findTokenByUser($userId)
    {
        return $this->findOneBy([
                'user' => $userId,
        ]);
    }
}