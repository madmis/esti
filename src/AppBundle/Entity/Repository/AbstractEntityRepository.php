<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class AbstractEntityRepository
 * @package AppBundle\Entity\Repository
 */
abstract class AbstractEntityRepository extends EntityRepository
{

}