<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\ResetPasswordToken;

/**
 * Class ResetPasswordTokenRepository
 * @package AppBundle\Entity\Repository
 */
class ResetPasswordTokenRepository extends AbstractEntityRepository
{
    /**
     * @param $token
     * @return null|ResetPasswordToken
     */
    public function findToken($token)
    {
        return $this->findOneBy([
            'token' => $token,
        ]);
    }
}