<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Tag;

/**
 * Class ProjectTagRepository
 * @package AppBundle\Entity\Repository
 */
class ProjectTagRepository extends AbstractEntityRepository
{
    /**
     * @param string $name
     * @return Tag
     */
    public function getByName($name)
    {
        return $this->findOneBy([
            'tag' => $name,
        ]);
    }

    /**
     * @param string $term
     * @return array|Tag[]
     */
    public function searchTag($term)
    {
        $term = strtolower($term);
        return $this->createQueryBuilder('t')
            ->andWhere('LOWER(t.tag) LIKE :term')
            ->setParameter(':term', "%{$term}%")
            ->getQuery()->getResult();
    }

}