<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Technology;

/**
 * Class TechnologyRepository
 * @package AppBundle\Entity\Repository
 */
class TechnologyRepository extends AbstractEntityRepository
{
    /**
     * @param string $name
     * @return Technology
     */
    public function getByName($name)
    {
        $res = $this->findOneBy([
            'name' => $name,
        ]);

        return $res;
    }

    /**
     * @param string $term
     * @return array|Technology[]
     */
    public function searchTechnology($term)
    {
        $term = strtolower($term);
        return $this->createQueryBuilder('t')
            ->andWhere('LOWER(t.name) LIKE :term')
            ->setParameter(':term', "%{$term}%")
            ->getQuery()->getResult();
    }
}