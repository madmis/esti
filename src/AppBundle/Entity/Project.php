<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Project
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ProjectRepository")
 * @ORM\Table(name="project")
 * @Gedmo\SoftDeleteable(fieldName="deleted", timeAware=false)
 * @Gedmo\Loggable
 * @JMS\ExclusionPolicy("none")
 */
class Project implements StringableInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="project.name.blank")
     * @Assert\Length(
     *      min = 10,
     *      max = 255,
     *      minMessage = "project.name.to_short",
     *      maxMessage = "project.name.to_long"
     * )
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="project.name.blank")
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="string", nullable=true, options={"comment":"Project public uri"})
     *
     * @var string
     */
    private $publicUri;

    /**
     * @ORM\Column(type="string", nullable=true, options={"comment":"Path (or uri) to project repository"})
     *
     * @var string
     */
    private $repository;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="date", nullable=true)
     * @JMS\Type("DateTime<'Y-m-d'>")
     *
     * @var \DateTime
     */
    private $startDate;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="date", nullable=true)
     * @JMS\Type("DateTime<'Y-m-d'>")
     *
     * @var \DateTime
     */
    private $endDate;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @JMS\Type("DateTime")
     *
     * @var \DateTime
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @JMS\Type("DateTime")
     *
     * @var \DateTime
     */
    private $updated;

    /**
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     * @JMS\Type("DateTime")
     *
     * @var \DateTime
     */
    private $deleted;

    /**
     * @ORM\ManyToMany(targetEntity="Tag", cascade={"persist"})
     * @ORM\JoinTable(name="project_tag",
     *      joinColumns={@ORM\JoinColumn(name="project_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id", onDelete="cascade")}
     * )
     * @Assert\Count( min="1", minMessage="project.tags.blank" )
     *
     * @var ArrayCollection|Tag[]
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="ProjectTask", mappedBy="project", cascade={"persist"})
     *
     * @var ArrayCollection|ProjectTask[]
     */
    private $tasks;

    /**
     * @ORM\ManyToMany(targetEntity="Technology", cascade={"persist"})
     * @ORM\JoinTable(name="project_technology",
     *      joinColumns={@ORM\JoinColumn(name="project_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="technology_id", referencedColumnName="id", onDelete="cascade")}
     * )
     * @Assert\Count( min="1", minMessage="project.technologies.blank" )
     * @Assert\Valid
     *
     * @var ArrayCollection|Technology[]
     */
    private $technologies;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="projects")
     * @JMS\Exclude
     *
     * @var User
     */
    private $user;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->tasks = new ArrayCollection();
        $this->technologies = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     *
     * @return $this
     */
    public function setStartDate(\DateTime $startDate = null)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     *
     * @return $this
     */
    public function setEndDate(\DateTime $endDate = null)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return Tag[]|ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param Tag[]|ArrayCollection $tags
     * @return $this
     */
    public function setTags(ArrayCollection $tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * @param Tag $tag
     * @return $this
     */
    public function addTag(Tag $tag)
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
        }

        return $this;
    }

    /**
     * @param Tag $tag
     * @return $this
     */
    public function removeTag(Tag $tag)
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    /**
     * @return ProjectTask[]|ArrayCollection
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param ProjectTask[]|ArrayCollection $tasks
     * @return $this
     */
    public function setTasks(ArrayCollection $tasks)
    {
        $this->tasks = $tasks;

        return $this;
    }

    /**
     * @param ProjectTask $task
     * @return $this
     */
    public function addTask(ProjectTask $task)
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks->add($task);
        }

        return $this;
    }

    /**
     * @param ProjectTask $task
     * @return $this
     */
    public function removeTask(ProjectTask $task)
    {
        if ($this->tasks->contains($task)) {
            $this->tasks->removeElement($task);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getPublicUri()
    {
        return $this->publicUri;
    }

    /**
     * @param string $publicUri
     * @return $this
     */
    public function setPublicUri($publicUri)
    {
        $this->publicUri = $publicUri;

        return $this;
    }

    /**
     * @return string
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param string $repository
     * @return $this
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;

        return $this;
    }

    /**
     * @return Technology[]|ArrayCollection
     */
    public function getTechnologies()
    {
        return $this->technologies;
    }

    /**
     * @param ArrayCollection $technologies
     */
    public function setTechnologies(ArrayCollection $technologies)
    {
        $this->technologies = $technologies;
    }

    /**
     * @param Technology $technology
     * @return $this
     */
    public function addTechnology(Technology $technology)
    {
        if (!$this->technologies->contains($technology)) {
            $this->technologies->add($technology);
        }

        return $this;
    }

    /**
     * @param Technology $technology
     * @return $this
     */
    public function removeTechnology(Technology $technology)
    {
        if ($this->technologies->contains($technology)) {
            $this->technologies->removeElement($technology);
        }

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     *
     * @return $this
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     *
     * @return $this
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param \DateTime $deleted
     *
     * @return $this
     */
    public function setDeleted(\DateTime $deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return null !== $this->deleted;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
}