<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\FlashMessage;
use AppBundle\Form\Type\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RegistrationController
 * @package AppBundle\Controller
 */
class RegistrationController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(new RegistrationFormType(), $user);

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $this->get('helper.registration')->register($user);

            $message = $this->get('translator')
                ->trans('message.register_successful', [], 'register');
            $this->addFlash(FlashMessage::SUCCESS, $message);

            return $this->redirectToRoute('app_security_confirm_email');
        }

        return $this->render('security/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function confirmAction(Request $request)
    {
        $tokenManager = $this->get('manager.confirmation_token');

        $errorMessage = null;
        if ($request->get('token')) {
            $token = $tokenManager->repo()
                ->findToken($request->get('token'));

            if ($token === null) {
                $message = $this->get('translator')
                    ->trans('error.token_invalid', [], 'register');
                $this->addFlash(FlashMessage::DANGER, $message);
            } else {
                $tokenManager->removeToken($token);

                $message = $this->get('translator')
                    ->trans('message.confirmation_successful', [], 'register');
                $this->addFlash(FlashMessage::SUCCESS, $message);

                $url = $this->generateUrl('app_security_login_form');
                return new RedirectResponse($url);
            }
        }

        return $this->render('security/confirmation_token.html.twig');
    }
}
