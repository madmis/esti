<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted(AuthenticatedVoter::IS_AUTHENTICATED_FULLY)) {
            return new RedirectResponse('/client');
        }

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..'),
        ]);
    }

    /**
     *
     */
    public function getUserAction()
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if (!$securityContext->isGranted(AuthenticatedVoter::IS_AUTHENTICATED_FULLY)) {
            return new JsonResponse([
                'error' => [
                    'httpCode' => 401,
                    'message' => 'Authentication required'
                ]
            ], 401);
        }

        $user = $this->getUser();
        return new JsonResponse([
            'data' => [
                'user' => [
                    'name' => 'Dimon',
                    'email' => 'dmac@ciklum.com',
                ]
            ]
        ], 200);
    }
}
