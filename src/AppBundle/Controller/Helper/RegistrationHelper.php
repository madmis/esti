<?php

namespace AppBundle\Controller\Helper;

use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Class RegistrationHelper
 * @package AppBundle\Controller\Helper
 */
class RegistrationHelper extends ContainerAware
{
    /**
     * @param User $user
     */
    public function register(User $user)
    {
        $password = $this->container->get('security.password_encoder')
            ->encodePassword($user, $user->getPlainPassword());
        $user->setPassword($password);

        $em = $this->container->get('manager.user')->em();
        $em->persist($user);
        $em->flush();

        $tokenManager = $this->container->get('manager.confirmation_token');
        $token = $tokenManager->createToken($user);
        $tokenManager->em()->persist($token);
        $tokenManager->em()->flush();

        $this->container->get('mail.handler')->sendConfirmationToken($token);
    }
}