<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Project;
use AppBundle\Form\Type\ProjectType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProjectController
 * @package AppBundle\Controller\Api
 */
class ProjectController extends AbstractApiController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function createProjectAction(Request $request)
    {
        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);

        $form->handleRequest($request);

        $technologies = $request->request->get('technologies');
        if ($technologies && is_array($technologies)) {
            $techManager = $this->get('manager.technology');
            foreach ($request->request->get('technologies') as $tech) {
                $exists = $techManager->repo()->getByName($tech);
                if (!$exists) {
                    $exists = $techManager->create($tech);
                }
                $form->getData()->addTechnology($exists);
            }
        }

        $tags = $request->request->get('tags');
        if ($tags && is_array($tags)) {
            $tagManager = $this->get('manager.project_tag');
            foreach ($request->request->get('tags') as $tag) {
                $exists = $tagManager->repo()->getByName($tag);
                if (!$exists) {
                    $exists = $tagManager->create($tag);
                }
                $form->getData()->addTag($exists);
            }
        }

        $response = $this->get('http.response_formatter');
        if (!$form->isValid()) {
            $errors = $form->getErrors(true);
            foreach ($errors as $error) {
                $response->addError($error->getMessage(), Response::HTTP_BAD_REQUEST);
            }

            return $response->jsonResponse(Response::HTTP_BAD_REQUEST);
        }

        $project->setUser($this->getUser());

        $em = $this->get('manager.project')->em();
        $em->persist($project);
        $em->flush();

        return $response
            ->setSuccess(['project' => $project])
            ->jsonResponse(Response::HTTP_OK);
    }

    /**
     * @param Request $request
     */
    public function updateProjectAction(Request $request)
    {
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function projectsListAction(Request $request)
    {
        $user = $this->getUser();
        $projects = $this->get('manager.project')
            ->repo()->getUserProjects($user);

        return $this->get('http.response_formatter')
            ->setSuccess(['projects' => $projects])
            ->jsonResponse(Response::HTTP_OK);
    }
}