<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\User;
use AppBundle\Form\Type\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RegistrationController
 * @package AppBundle\Controller\Api
 */
class RegistrationController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function registerAction(Request $request)
    {
        $response = $this->get('http.response_formatter');

        $user = new User();
        $form = $this->createForm(new RegistrationFormType(), $user);

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $this->get('helper.registration')->register($user);

            $message = $this->get('translator')
                ->trans('message.register_successful', [], 'register');

            return $response
                ->setSuccess([
                    'message' => $message,
                ])->jsonResponse(Response::HTTP_OK);
        }

        $errors = $form->getErrors(true);
        foreach ($errors as $error) {
            $response->addError(
                $error->getMessage(),
                Response::HTTP_BAD_REQUEST
            );
        }

        return $response->jsonResponse(Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function confirmEmailAction(Request $request)
    {
        $token = $request->request->get('token');
        $actualCsrf = $request->request->get('_token');
        $expectedCsrf = $this->container->get('security.csrf.token_manager')
            ->getToken('confirm-email');

        $response = $this->container->get('http.response_formatter');

        if ($actualCsrf != $expectedCsrf) {
            return $response->addError('Invalid CSRF token. Try to reload page.')
                ->jsonResponse(Response::HTTP_BAD_REQUEST);
        }

        $token = $this->container->get('manager.confirmation_token')
            ->findToken($token);
        if (!$token) {
            return $response->addError('Invalid confirmation token.')
                ->jsonResponse(Response::HTTP_BAD_REQUEST);
        }

        $num = $this->container->get('manager.confirmation_token')
            ->deleteUserTokens($token->getUser());

        return $response->setSuccess([])
            ->jsonResponse(Response::HTTP_OK);
    }
}