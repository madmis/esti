<?php

namespace AppBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CsrfController
 * @package AppBundle\Controller\Api
 */
class CsrfController extends Controller
{
    /**
     * @param Request $request
     * @param $intention
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function generateCsrfAction(Request $request, $intention)
    {
        $csrf = $this->get('security.csrf.token_manager');
        $token = $csrf->refreshToken($intention);

        return $this->get('http.response_formatter')
            ->setSuccess([
                'csrf_token' => $token->getValue(),
            ])->jsonResponse(Response::HTTP_OK);
    }
}