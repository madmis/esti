<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AbstractApiController
 * @package AppBundle\Controller\Api
 */
class AbstractApiController extends Controller
{
    /**
     * @return User
     */
    public function getUser()
    {
        return parent::getUser();
    }
}