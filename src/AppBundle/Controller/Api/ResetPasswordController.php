<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\ResetPasswordToken;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ResetPasswordController
 * @package AppBundle\Controller\Api
 */
class ResetPasswordController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function requestAction(Request $request)
    {
        $response = $this->get('http.response_formatter');

        $email = $request->request->get('email');
        /** @var User $user */
        $user = $this->get('manager.user')->repo()->findOneBy([
            'email' => $email,
        ]);

        // if email invalid - do nothing
        $ttl = $this->container->getParameter('reset_password_token_ttl');
        if ($user) {
            $manager = $this->get('manager.reset_password_token');
            $token = $manager->createToken($user, $ttl);
            $manager->em()->persist($token);
            $manager->em()->flush();

            $this->get('mail.handler')->sendResetPasswordToken($token);
        }

        $message = $this->get('translator')
            ->transChoice('message.tokent_sent', $ttl, ['%time%' => $ttl], 'reset_pass');

        return $response
            ->setSuccess([
                'message' => $message,
            ])->jsonResponse(Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param string $token
     * @return Response
     */
    public function resetAction(Request $request, $token)
    {
        $response = $this->get('http.response_formatter');

        $csrf = $request->request->get('_token');
        if (!$this->isCsrfTokenValid('reset-password', $csrf)) {
            $error = $this->get('translator')
                ->trans('error.csrf_token_invalid', [], 'reset_pass');
            $response->addError($error);

            return $response->jsonResponse(Response::HTTP_BAD_REQUEST);
        }

        /** @var ResetPasswordToken $entity */
        $entity = $this->get('manager.reset_password_token')
            ->repo()->findToken($token);
        if (!$token || !$entity || $entity->getExpires() < new \DateTime()) {
            $error = $this->get('translator')
                ->trans('error.token_invalid', [], 'reset_pass');
            $response->addError($error);

            return $response->jsonResponse(Response::HTTP_BAD_REQUEST);
        }

        $passFirst = $request->request->get('password');
        $passSecond = $request->request->get('confirmPassword');

        if ($passFirst && $passSecond && $passFirst == $passSecond) {
            $user = $entity->getUser();
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $passFirst);
            $user->setPassword($password);

            $em = $this->get('manager.user')->em();
            $em->persist($user);
            $em->flush();

            $em->remove($entity);
            $em->flush();

            $message = $this->get('translator')
                ->trans('message.password_changed', [], 'reset_pass');

            return $response
                ->setSuccess([
                    'message' => $message,
                ])->jsonResponse(Response::HTTP_OK);
        }

        $error = $this->get('translator')
            ->trans('error.password_mismatch', [], 'reset_pass');
        $response->addError($error);
        return $response->jsonResponse(Response::HTTP_BAD_REQUEST);
    }
}