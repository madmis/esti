<?php

namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DictionaryController
 * @package AppBundle\Controller\Api
 */
class DictionaryController extends AbstractApiController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function searchProjectTagsAction(Request $request)
    {
        $term = $request->query->get('term');

        $result = [];
        if ($term) {
            $result = $this->get('manager.project_tag')->repo()
                ->searchTag($term);
        }

        return $this->get('http.response_formatter')
            ->setSuccess($result)
            ->jsonResponse(Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function searchTechnologiesAction(Request $request)
    {
        $term = $request->query->get('term');

        $result = [];
        if ($term) {
            $result = $this->get('manager.technology')->repo()
                ->searchTechnology($term);
        }

        return $this->get('http.response_formatter')
            ->setSuccess($result)
            ->jsonResponse(Response::HTTP_OK);
    }
}
