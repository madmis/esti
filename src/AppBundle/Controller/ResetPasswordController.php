<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ResetPasswordToken;
use AppBundle\Entity\User;
use AppBundle\FlashMessage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ResetPasswordController
 * @package AppBundle\Controller
 */
class ResetPasswordController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function requestAction(Request $request)
    {
        $email = $request->request->get('email');
        $error = '';
        if ($request->isMethod('POST')) {
            $csrf = $request->request->get('_csrf_token');
            if ($this->isCsrfTokenValid('request_reset', $csrf)) {
                if ($email) {
                    $this->generateResetToken($email);
                    return $this->redirectToRoute('app_security_request_reset_pass');
                }
            } else {
                $error = $this->get('translator')
                    ->trans('error.csrf_token_invalid', [], 'reset_pass');
            }
        }

        return $this->render('security/reset_password_request.html.twig', [
            'email' => $email,
            'error' => $error,
        ]);
    }

    /**
     * @param Request $request
     * @param string $token
     * @return Response
     */
    public function resetAction(Request $request, $token)
    {
        $error = '';

        /** @var ResetPasswordToken $entity */
        $entity = $this->get('manager.reset_password_token')
            ->repo()->findToken($token);
        if (!$token || !$entity || $entity->getExpires() < new \DateTime()) {
            $error = $this->get('translator')
                ->trans('error.token_invalid', [], 'reset_pass');
        } elseif ($request->isMethod('POST')) {
            $passFirst = $request->request->get('plainPassword_first');
            $passSecond = $request->request->get('plainPassword_second');

            if ($passFirst && $passSecond) {
                if ($passFirst != $passSecond) {
                    $error = $this->get('translator')
                        ->trans('error.password_mismatch', [], 'reset_pass');
                } else {
                    $user = $entity->getUser();
                    $password = $this->get('security.password_encoder')
                        ->encodePassword($user, $passFirst);
                    $user->setPassword($password);

                    $em = $this->get('manager.user')->em();
                    $em->persist($user);
                    $em->flush();

                    $message = $this->get('translator')
                        ->trans('message.password_changed', [], 'reset_pass');
                    $this->addFlash(FlashMessage::SUCCESS, $message);

                    return $this->redirectToRoute('app_security_login_form');

                }
            }
        }

        return $this->render('security/reset_password.html.twig', [
            'error' => $error,
            'token' => $token,
        ]);
    }

    /**
     * @param string $email
     */
    private function generateResetToken($email)
    {
        /** @var User $user */
        $user = $this->get('manager.user')->repo()->findOneBy([
            'email' => $email,
        ]);

        // if email invalid - do nothing
        if ($user) {
            $ttl = $this->container->getParameter('reset_password_token_ttl');
            $manager = $this->get('manager.reset_password_token');
            $token = $manager->createToken($user, $ttl);
            $manager->em()->persist($token);
            $manager->em()->flush();

            $this->get('mail.handler')->sendResetPasswordToken($token);

            $message = $this->get('translator')
                ->transChoice('message.tokent_sent', $ttl, ['%time%' => $ttl], 'reset_pass');
            $this->addFlash(FlashMessage::SUCCESS, $message);
        }
    }
}