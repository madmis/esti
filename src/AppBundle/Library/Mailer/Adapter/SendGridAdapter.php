<?php

namespace AppBundle\Library\Mailer\Adapter;

use AppBundle\Library\Mailer\MailAdapterInterface;
use AppBundle\Library\Mailer\Message\MessageInterface;
use AppBundle\Library\Mailer\Message\SendGridMessage;

/**
 * Class SendGridAdapter
 * @package AppBundle\Library\Mailer\Adapter
 */
class SendGridAdapter implements MailAdapterInterface
{
    /**
     * @var \SendGrid
     */
    private $mailer;

    /**
     * Default from email
     * @var string
     */
    private $fromDefault;

    /**
     * @param \SendGrid $mailer
     * @param string $fromDefault default from email
     */
    public function __construct(\SendGrid $mailer, $fromDefault)
    {
        $this->mailer = $mailer;
        $this->fromDefault = $fromDefault;
    }

    /**
     * @param MessageInterface $message
     * @return \stdClass
     * @throws \SendGrid\Exception
     */
    public function send(MessageInterface $message)
    {
        return $this->mailer->send($message->getMessage());
    }

    /**
     * @inheritdoc
     */
    public function createMessage()
    {
        $message = new SendGridMessage();

        if ($this->getFromDefault()) {
            $message->setFrom($this->getFromDefault());
        }

        return $message;
    }

    /**
     * Default from email
     * @return string
     */
    public function getFromDefault()
    {
        return $this->fromDefault;
    }
}