<?php

namespace AppBundle\Library\Mailer\Adapter;

use AppBundle\Library\Mailer\MailAdapterInterface;
use AppBundle\Library\Mailer\Message\MessageInterface;
use AppBundle\Library\Mailer\Message\SwiftMailerMessage;

/**
 * Class SwiftMailerAdapter
 * @package AppBundle\Library\Mailer\Adapter
 */
class SwiftMailerAdapter implements MailAdapterInterface
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * Default from email
     * @var string
     */
    private $fromDefault;

    /**
     * @param \Swift_Mailer $mailer
     * @param string $fromDefault default from email
     */
    public function __construct(\Swift_Mailer $mailer, $fromDefault)
    {
        $this->mailer = $mailer;
        $this->fromDefault = $fromDefault;
    }

    /**
     * Create message
     *
     * @return MessageInterface
     */
    public function createMessage()
    {
        $message = new SwiftMailerMessage();

        if ($this->getFromDefault()) {
            $message->setFrom($this->getFromDefault());
        }

        return $message;
    }

    /**
     * Send message
     *
     * @param MessageInterface $message
     * @return mixed
     */
    public function send(MessageInterface $message)
    {
        return $this->mailer->send($message->getMessage());
    }

    /**
     * Default from email
     * @return string
     */
    public function getFromDefault()
    {
        return $this->fromDefault;
    }
}