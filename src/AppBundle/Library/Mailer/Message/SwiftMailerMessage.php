<?php

namespace AppBundle\Library\Mailer\Message;

/**
 * Class SwiftMailerMessage
 * @package AppBundle\Library\Mailer\Message
 */
class SwiftMailerMessage implements MessageInterface
{
    /**
     * @var \Swift_Message
     */
    private $message;

    public function __construct()
    {
        $this->message = \Swift_Message::newInstance();
    }

    /**
     * @return \Swift_Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @inheritdoc
     */
    public function setTo($addresses, $name = null)
    {
        $this->message->setTo($addresses, $name);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setFrom($address, $name = null)
    {
        $this->message->setFrom($address, $name);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setSubject($subject)
    {
        $this->message->setSubject($subject);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setText($text)
    {
        $this->message->setBody($text, 'text/plain');

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setHtml($html)
    {
        $this->message->setBody($html, 'text/html');
    }
}