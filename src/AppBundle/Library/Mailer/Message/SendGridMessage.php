<?php

namespace AppBundle\Library\Mailer\Message;

/**
 * Class SendGridMessage
 * @package AppBundle\Library\Mailer\Message
 */
class SendGridMessage implements MessageInterface
{
    /**
     * @var \SendGrid\Email
     */
    private $message;

    public function __construct()
    {
        $this->message = new \SendGrid\Email();
    }

    /**
     * @return \SendGrid\Email
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @inheritdoc
     */
    public function setTo($addresses, $name = null)
    {
        if (!is_array($addresses)) {
            $this->message->addTo($addresses, $name);
        } else {
            $emails = [];
            $names = [];
            foreach ($addresses as $toEmail => $toName) {
                $emails[] = is_numeric($toEmail) ? $toName : $toEmail;
                $names[] = $toName;
            }

            $this->message->addTo($emails, $names);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setFrom($address, $name = null)
    {
        $this->message->setFrom($address);

        if ($name) {
            $this->message->setFromName($name);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setSubject($subject)
    {
        $this->message->setSubject($subject);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setText($text)
    {
        $this->message->setText($text);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setHtml($html)
    {
        $this->message->setHtml($html);

        return $this;
    }
}