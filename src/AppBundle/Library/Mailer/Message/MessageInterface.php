<?php

namespace AppBundle\Library\Mailer\Message;

/**
 * Interface MessageInterface
 * @package AppBundle\Library\Mailer\Message
 */
interface MessageInterface
{
    /**
     * Get original provider message
     * @return mixed
     */
    public function getMessage();

    /**
     * Set the to addresses of this message.
     *
     * If multiple recipients will receive the message an array should be used.
     * Example: array('receiver@domain.org', 'other@domain.org' => 'A name')
     *
     * If $name is passed and the first parameter is a string, this name will be
     * associated with the address.
     *
     * @param string|array $addresses
     * @param string $name optional
     *
     * @return MessageInterface
     */
    public function setTo($addresses, $name = null);

    /**
     * Set the from address of this message.
     *
     * @param string $address
     * @param string $name optional
     *
     * @return MessageInterface
     */
    public function setFrom($address, $name = null);

    /**
     * Set the subject of this message.
     *
     * @param string $subject
     *
     * @return MessageInterface
     */
    public function setSubject($subject);

    /**
     * Set message text
     *
     * @param string $text
     *
     * @return MessageInterface
     */
    public function setText($text);

    /**
     * Set message html
     *
     * @param string $html
     *
     * @return MessageInterface
     */
    public function setHtml($html);
}