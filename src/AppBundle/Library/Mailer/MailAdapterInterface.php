<?php

namespace AppBundle\Library\Mailer;

use AppBundle\Library\Mailer\Message\MessageInterface;

/**
 * Interface MailAdapterInterface
 * @package AppBundle\Library\Mailer
 */
interface MailAdapterInterface
{
    /**
     * Create message
     *
     * @return MessageInterface
     */
    public function createMessage();

    /**
     * Send message
     *
     * @param MessageInterface $message
     * @return mixed
     */
    public function send(MessageInterface $message);

    /**
     * Default from email
     * @return string
     */
    public function getFromDefault();
}