<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RegistrationFormType
 * @package AppBundle\Form\Type
 */
class RegistrationFormType extends AbstractType
{
    const INTENTION = 'registration';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', [
                'label' => false,
                'translation_domain' => 'register',
                'attr' => [
                    'placeholder' => 'form.email',
                ],
            ])
            ->add('plainPassword', 'repeated', [
                'type'            => 'password',
                'options'         => ['translation_domain' => 'register'],
                'first_options'   => [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'form.password',
                    ],
                ],
                'second_options'  => [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'form.password_confirmation',
                    ],
                ],
                'invalid_message' => 'registration.password.mismatch',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\User',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'intention'  => self::INTENTION,
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_registration';
    }
}