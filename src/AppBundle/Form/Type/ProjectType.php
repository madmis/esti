<?php

namespace AppBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class Projectype
 * @package AppBundle\Form\Type
 */
class ProjectType extends AbstractType
{
    const INTENTION = 'project';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextType::class)
            ->add('publicUri', UrlType::class)
            ->add('repository', TextType::class)
            ->add('startDate', DateType::class)
            ->add('endDate', DateType::class)
            ->add('tasks', CollectionType::class, [
                'entry_type'   => ProjectTaskType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true,
            ])
            ->add('tags', EntityType::class, [
                'class'    => 'AppBundle\Entity\Tag',
                'multiple' => true,
            ])
//            ->add('tags', CollectionType::class, [
//                'entry_type'   => ProjectTagType::class,
//                'allow_add'    => true,
//                'by_reference' => true,
//                'allow_delete' => true,
//            ])
            ->add('technologies', EntityType::class, [
                'class'    => 'AppBundle\Entity\Technology',
                'multiple' => true,
            ]);
//            ->add('technologies', CollectionType::class, [
//                'entry_type'   => TechnologyType::class,
//                'allow_add'    => true,
//                'by_reference' => false,
//                'allow_delete' => true,
//            ]);

    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'      => 'AppBundle\Entity\Project',
            'csrf_protection' => false,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'project';
    }
}