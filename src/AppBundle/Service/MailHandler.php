<?php

namespace AppBundle\Service;

use AppBundle\Entity\ConfirmationToken;
use AppBundle\Entity\ResetPasswordToken;
use AppBundle\Entity\User;
use AppBundle\Library\Mailer\MailAdapterInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class MailHandler
 * @package AppBundle\Service
 */
class MailHandler extends ContainerAware
{
    /**
     * @var MailAdapterInterface[]
     */
    private $mailProviders;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var MailAdapterInterface
     */
    private $provider;

    /**
     * @param MailAdapterInterface $publicMailProvider
     * @param MailAdapterInterface $systemMailProvider
     * @param UrlGeneratorInterface $router
     * @param \Twig_Environment $twig
     */
    public function __construct(
        MailAdapterInterface $publicMailProvider,
        MailAdapterInterface $systemMailProvider,
        UrlGeneratorInterface $router,
        \Twig_Environment $twig,
        TranslatorInterface $trans
    ) {
        $this->mailProviders = [
            'public' => $publicMailProvider,
            'system' => $systemMailProvider,
        ];
        $this->router = $router;
        $this->twig = $twig;
        $this->translator = $trans;

        $this->provider = $publicMailProvider;
    }

    /**
     * @param string $id
     * @param array $parameters
     * @return string
     */
    private function trans($id, array $parameters = [])
    {
        return $this->translator->trans($id, $parameters, 'mail');
    }

    /**
     * Use public mailer
     */
    public function usePublic()
    {
        $this->provider = $this->mailProviders['public'];
    }

    /**
     * Use system mailer
     */
    public function useSystem()
    {
        $this->provider = $this->mailProviders['system'];
    }

    /**
     * @param ConfirmationToken $token
     */
    public function sendConfirmationToken(ConfirmationToken $token)
    {
        $message = $this->provider->createMessage();
        $confirmationUrl = sprintf(
            '%s?token=%s',
            $this->container->getParameter('cl.security.confirm_email'),
            $token->getToken()
        );

        $parameters = [
            'user' => $token->getUser(),
            'confirmationUrl' => $confirmationUrl,
        ];

        $text = $this->twig->render('mail/text/confirmation_token.txt.twig', $parameters);
        $html = $this->twig->render('mail/html/confirmation_token.html.twig', $parameters);

        $message
            ->setTo($token->getUser()->getEmail())
            ->setSubject($this->trans('confirmation.subject'))
            ->setText($text)
            ->setHtml($html);

        $this->provider->send($message);
    }

    /**
     * @param ResetPasswordToken $token
     */
    public function sendResetPasswordToken(ResetPasswordToken $token)
    {
        $message = $this->provider->createMessage();
        $resetUrl = sprintf(
            '%s/%s',
            $this->container->getParameter('cl.security.reset_password'),
            $token->getToken()
        );
        $parameters = [
            'user' => $token->getUser(),
            'resetUrl' => $resetUrl,
        ];

        $text = $this->twig->render('mail/text/reset_password.txt.twig', $parameters);
        $html = $this->twig->render('mail/html/reset_password.html.twig', $parameters);

        $message
            ->setTo($token->getUser()->getEmail())
            ->setSubject($this->trans('reset_password.subject'))
            ->setText($text)
            ->setHtml($html);

        $this->provider->send($message);
    }
}