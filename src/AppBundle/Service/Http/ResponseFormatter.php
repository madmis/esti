<?php

namespace AppBundle\Service\Http;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ResponseFormatter
 * @package AppBundle\Service\Http
 */
class ResponseFormatter
{
    /**
     * @var ArrayCollection
     */
    private $errors;

    /**
     * @var array
     */
    private $success;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * ResponseFormatter constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->clean();
        $this->serializer = $serializer;
    }

    /**
     * @return $this
     */
    private function clean()
    {
        $this->errors = new ArrayCollection();
        $this->success = [
            'messages' => [],
            'data'     => [],
        ];

        return $this;
    }

    /**
     * Add error to response
     *
     * @param string $message
     * @param int $statusCode http status code
     * @return $this
     */
    public function addError($message, $statusCode = Response::HTTP_BAD_REQUEST)
    {
        $this->errors->add([
            'statusCode' => $statusCode,
            'message'    => $message,
        ]);

        return $this;
    }

    /**
     * @param array $data
     * @param string|array $messages
     * @return $this
     */
    public function setSuccess(array $data, $messages = [])
    {
        if (!is_array($messages)) {
            $messages = [$messages];
        }

        $this->success = [
            'messages' => $messages,
            'data'     => $data,
        ];

        return $this;
    }

    /**
     * @param int $statusCode http status code
     *          {@see Symfony\Component\HttpFoundation\Response::HTTP_OK}
     *          {@see Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT}
     * @param array $headers
     * @return JsonResponse
     */
    public function jsonResponse($statusCode, array $headers = [])
    {
        $response = $this->success;
        if (!$this->errors->isEmpty()) {
            $response['errors'] = $this->errors->toArray();
        }

        $context = new SerializationContext();
        $context->setSerializeNull(true);
        $result = $this->serializer->serialize($response, 'json', $context);

        $response = new JsonResponse();
        $response->setContent($result);
        $response->setStatusCode($statusCode);
        $response->headers->add($headers);

        return $response;
    }
}