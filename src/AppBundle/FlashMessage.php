<?php

namespace AppBundle;

final class FlashMessage
{
    const SUCCESS = 'success';
    const INFO = 'info';
    const DANGER = 'danger';
    const WARNING = 'warning';
}